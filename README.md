## Transportasi Microservice (TK Adpro A7)
[![pipeline status](https://gitlab.com/serbaguna-microservices/transportasi/badges/master/pipeline.svg)](https://gitlab.com/serbaguna-microservices/transportasi/-/commits/master)
[![coverage report](https://gitlab.com/serbaguna-microservices/transportasi/badges/master/coverage.svg)](https://gitlab.com/serbaguna-microservices/transportasi/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=serbaguna-microservices_transportasi&metric=alert_status)](https://sonarcloud.io/dashboard?id=serbaguna-microservices_transportasi)

Dibuat oleh Steven - 1906293322 sebagai bagian dari [Tugas Kelompok Adpro Kelompok A7](https://gitlab.com/serbaguna-microservices)

## SonarCloud
Proyek ini juga di-scan untuk Code Smells, Bugs, dan Vulnerabilities dengan SonarCloud yang bisa dilihat [link berikut](https://sonarcloud.io/dashboard?id=serbaguna-microservices_transportasi)

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=serbaguna-microservices_transportasi)](https://sonarcloud.io/dashboard?id=serbaguna-microservices_transportasi)

## Links dan Line ID
Microservice ini di deploy di https://serbaguna-transport.herokuapp.com/

Line ID OA: `@109tpdsd`