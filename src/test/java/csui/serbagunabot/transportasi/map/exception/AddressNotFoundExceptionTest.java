package csui.serbagunabot.transportasi.map.exception;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AddressNotFoundExceptionTest {

    AddressNotFoundException addressNotFoundException = new AddressNotFoundException();

    @Test
    void testAddressNotFoundIsAnExceptionToo() {
        Assertions.assertTrue(addressNotFoundException instanceof Exception);
    }

}
