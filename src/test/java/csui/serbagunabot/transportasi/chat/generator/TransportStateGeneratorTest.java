package csui.serbagunabot.transportasi.chat.generator;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.chat.states.common.NoTransportState;
import csui.serbagunabot.transportasi.chat.states.helikopter.HelicopterConfirmState;
import csui.serbagunabot.transportasi.chat.states.ojek.HealthProtocolState;
import csui.serbagunabot.transportasi.chat.states.taxi.AskTaxiModeState;
import csui.serbagunabot.transportasi.model.TransportUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

@ExtendWith(MockitoExtension.class)
class TransportStateGeneratorTest {

    @Mock
    TransportUser transportUser;

    @Mock
    AutowireCapableBeanFactory autowireCapableBeanFactory;

    @InjectMocks
    TransportStateGenerator transportStateGenerator;

    @Test
    void testGenerateStartingStates() {
        TransportState resultState = transportStateGenerator.generateStartingStates(transportUser);

        Assertions.assertTrue(resultState instanceof NoTransportState);
    }

    @Test
    void testGenerateOjekStates() {
        TransportState resultState = transportStateGenerator.generateOjekStates(transportUser);

        Assertions.assertTrue(resultState instanceof HealthProtocolState);
    }

    @Test
    void testGenerateHelicopterStates() {
        TransportState resultState = transportStateGenerator.generateHeliStates(transportUser);

        Assertions.assertTrue(resultState instanceof HelicopterConfirmState);
    }

    @Test
    void testGenerateTaxiStates() {
        TransportState resultState = transportStateGenerator.generateTaxiStates(transportUser);
        Assertions.assertTrue(resultState instanceof AskTaxiModeState);
    }

}
