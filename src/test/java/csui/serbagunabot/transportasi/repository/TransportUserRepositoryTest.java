package csui.serbagunabot.transportasi.repository;

import csui.serbagunabot.transportasi.chat.generator.TransportStateGenerator;
import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TransportUserRepositoryTest {

    @Mock
    private TransportStateGenerator transportStateGenerator;

    @Mock
    private TransportState arbitraryState;

    @InjectMocks
    private TransportUserRepositoryImpl transportUserRepository;

    private TransportUser transportUser;
    private static final String USER_ID_SAMPLE = "ABCXYZ";
    private static final String NON_EXISTENT_ID = "123456";

    @BeforeEach
    public void setUp() {
        transportUser = new TransportUser();
    }

    @Test
    void testRepositoryCreatedUser() {
        transportUser = transportUserRepository
                .createUserFromId(USER_ID_SAMPLE);

        Assertions.assertNotNull(transportUser);
    }

    @Test
    void testFindCreatedUserShouldExist() {
        transportUserRepository
                .createUserFromId(USER_ID_SAMPLE);

        transportUser = transportUserRepository.findByUserId(USER_ID_SAMPLE);

        Assertions.assertNotNull(transportUser);
    }

    @Test
    void testNonExistentUserShouldReturnNull() {
        transportUser = transportUserRepository
                .findByUserId(NON_EXISTENT_ID);

        Assertions.assertNull(transportUser);
    }

}
