package csui.serbagunabot.transportasi.chat.states.helikopter;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HelicopterDestinationStateTest {

    @Mock
    TransportState mockNextState;

    TransportUser transportUser = new TransportUser();

    @InjectMocks
    HelicopterDestinationState helicopterDestinationState =
        new HelicopterDestinationState(transportUser);

    private static final String RELIST_MSG = "list";
    private static final String INVALID_MSG = "abc";
    private static final String PICKUP_SAMPLE = "North Jakarta";

    private static final String ASK_LIST_MSG = "List dari tujuan tersedia sebagai berikut:\n";

    private static final String AIRPORT_LIST = "1) Soekarno-Hatta International Airport\n"
        + "2) Halim Perdanakusuma International Airport\n"
        + "3) Kemayoran International Airport\n"
        + "4) Bandar Udara Pondok Cabe\n"
        + "Ketik '!transport n' untuk memilih, dengan n adalah nomor pilihan anda.";

    private static final String INVALID_REPLY = "Pilihan tidak dikenal!\n"
        + "Ketik '!transport list' untuk menampilkan list tujuan yang tersedia.";

    private static final String OPENING_REPLY = "Silahkan input bandara tujuan "
        + "anda, berikut tujuan yang tersedia:\n";

    @BeforeEach
    void setUp() {
        helicopterDestinationState.setNextState(mockNextState);
        transportUser.setCurrentState(helicopterDestinationState);
        transportUser.setPickupAddress(PICKUP_SAMPLE);
    }

    @ParameterizedTest
    @CsvSource({
        "'1', 'Soekarno-Hatta International Airport'",
        "'2', 'Halim Perdanakusuma International Airport'",
        "'3', 'Kemayoran International Airport'",
        "'4', 'Bandar Udara Pondok Cabe'",
    })
    void testHandleMessageValidLocations(String input, String expectedDest) {
        String response = helicopterDestinationState.handleMessage(input);
        Assertions.assertEquals(transportUser.getDestinationAddress(), expectedDest);
        Assertions.assertEquals(transportUser.getCurrentState(), mockNextState);
    }

    @Test
    void testHandleRelistChoicesMessage() {
        String response = helicopterDestinationState.handleMessage(RELIST_MSG);
        Assertions.assertEquals(ASK_LIST_MSG + AIRPORT_LIST, response);
    }

    @Test
    void testHandleInvalidMessage() {
        String response = helicopterDestinationState.handleMessage(INVALID_MSG);
        Assertions.assertEquals(INVALID_REPLY, response);
    }

    @Test
    void testGetOpeningMessage() {
        Assertions.assertEquals(helicopterDestinationState.getOpeningMessage(),
            OPENING_REPLY + AIRPORT_LIST);
    }

}
