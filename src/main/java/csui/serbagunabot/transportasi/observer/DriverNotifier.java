package csui.serbagunabot.transportasi.observer;

import csui.serbagunabot.transportasi.feign.LinePushFeign;
import csui.serbagunabot.transportasi.map.MapUtil;
import csui.serbagunabot.transportasi.map.exception.AddressNotFoundException;
import csui.serbagunabot.transportasi.model.Driver;
import csui.serbagunabot.transportasi.model.TransportUser;
import csui.serbagunabot.transportasi.model.line.LineRequest;
import csui.serbagunabot.transportasi.repository.DriverRepository;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DriverNotifier {

    @Autowired
    DriverRepository driverRepository;

    @Autowired
    LinePushFeign linePushFeign;

    @Autowired
    MapUtil mapUtil;

    /**
     * Get a driver based on the transport user's order.
     */
    public Driver notifyOneDriverForUser(TransportUser transportUser) {
        List<Driver> driverList = driverRepository
            .findAllByVehicleType(transportUser.getTransportMode().getAlias());

        if (!driverList.isEmpty()) {
            String targetId = driverList.get(0).getLineUserId();
            String message = buildTransportUserMessage(transportUser);
            linePushFeign.sendPushMessage(new LineRequest(targetId, message));
            return driverList.get(0);
        }
        return null;
    }

    private String buildTransportUserMessage(TransportUser transportUser) {
        double dist;
        try {
            dist = mapUtil.getDrivingDistance(transportUser
                .getPickupAddress(), transportUser.getDestinationAddress());
        } catch (AddressNotFoundException | IOException e) {
            dist = 0;
        }

        long price = Math.round(dist) * transportUser.getTransportMode().getCostPerKm();

        return "Anda mendapat order untuk " + transportUser.getTransportMode().getAlias() + "\n"
            + "dari \n\n"
            + transportUser.getPickupAddress() + "\n\n"
            + "menuju \n\n"
            + transportUser.getDestinationAddress() + "\n\n"
            + "untuk ongkos sebesar Rp. " + NumberFormat
            .getNumberInstance(Locale.US).format(price) + ".00";
    }

}
