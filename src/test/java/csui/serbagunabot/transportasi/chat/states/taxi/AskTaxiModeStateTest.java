package csui.serbagunabot.transportasi.chat.states.taxi;

import static org.mockito.Mockito.when;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportMode;
import csui.serbagunabot.transportasi.model.TransportUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AskTaxiModeStateTest {

    @Mock
    TransportState arbitraryState;

    private static final String TAXI_LARGE_MSG = "large";
    private static final String TAXI_STD_MSG = "standard";
    private static final String SAMPLE_REPLY = "xyz";
    private static final String INVALID_MSG = "abc";

    private static final String INVALID_CHOICE_REPLY = "Pilihan tidak dikenal!\n"
        + "Ketik '!transport standard' atau '!transport large' untuk "
        + "memilih ukuran taksi yang anda akan pesan.";

    private static final String OPENING_REPLY = "Silahkan memilih ukuran mobil "
        + "taksi dengan mengetik:\n"
        + "-) '!transport standard' untuk ukuran standar (2-3 orang)\n"
        + "-) '!transport large' untuk ukuran large (4-5 orang)";


    private TransportUser transportUser;
    private AskTaxiModeState askTaxiModeState;

    @BeforeEach
    void setUp() {
        transportUser = new TransportUser();
        askTaxiModeState = new AskTaxiModeState(transportUser);
        askTaxiModeState.setNextState(arbitraryState);
        transportUser.setCurrentState(askTaxiModeState);
    }

    @Test
    void testTaxiLargeMessage() {
        when(arbitraryState.getOpeningMessage()).thenReturn(SAMPLE_REPLY);
        String response = askTaxiModeState.handleMessage(TAXI_LARGE_MSG);

        Assertions.assertEquals(SAMPLE_REPLY, response);
        Assertions.assertEquals(TransportMode.TAXI_LARGE, transportUser.getTransportMode());
    }

    @Test
    void testTaxiStandardMessage() {
        when(arbitraryState.getOpeningMessage()).thenReturn(SAMPLE_REPLY);
        String response = askTaxiModeState.handleMessage(TAXI_STD_MSG);

        Assertions.assertEquals(SAMPLE_REPLY, response);
        Assertions.assertEquals(TransportMode.TAXI_STANDARD, transportUser.getTransportMode());
    }

    @Test
    void testTaxiInvalidMessage() {
        String response = askTaxiModeState.handleMessage(INVALID_MSG);

        Assertions.assertEquals(INVALID_CHOICE_REPLY, response);
    }

    @Test
    void testOpeningMessage() {
        Assertions.assertEquals(OPENING_REPLY, askTaxiModeState.getOpeningMessage());
    }

}
