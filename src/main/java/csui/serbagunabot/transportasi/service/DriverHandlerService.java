package csui.serbagunabot.transportasi.service;

import csui.serbagunabot.transportasi.model.Driver;
import csui.serbagunabot.transportasi.repository.DriverStateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DriverHandlerService {

    @Autowired
    DriverStateRepository driverStateRepository;

    Logger logger = LoggerFactory.getLogger("Driver Logging");

    /**
     * Handles all incoming !driver message based on the state.
     */
    public String handleMessage(String message, String userId) {
        String cleanMessage = message.replace("!driver", "").trim();
        logger.info("Received driver message {} from {}", message, userId);

        Driver driver = driverStateRepository.findByUserId(userId);

        if (driver == null) {
            driver = driverStateRepository.createUserFromId(userId);
        }

        return driver.respond(cleanMessage);
    }


}
