package csui.serbagunabot.transportasi.chat.states.common;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import csui.serbagunabot.transportasi.chat.generator.TransportStateGenerator;
import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.map.MapUtil;
import csui.serbagunabot.transportasi.map.exception.AddressNotFoundException;
import csui.serbagunabot.transportasi.model.Driver;
import csui.serbagunabot.transportasi.model.TransportMode;
import csui.serbagunabot.transportasi.model.TransportUser;
import csui.serbagunabot.transportasi.observer.DriverNotifier;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ConfirmOrderStateTest {


    private TransportUser transportUser = new TransportUser();

    private Driver driver;

    @Mock
    DriverNotifier driverNotifier;

    @Mock
    private TransportState mockReinputState;

    @Mock
    private TransportState mockArbitraryState;

    @Mock
    private MapUtil mapUtil;

    @Mock
    private TransportStateGenerator transportStateGenerator;

    @InjectMocks
    private ConfirmOrderState confirmOrderState = new ConfirmOrderState(transportUser);

    private static final TransportMode TRANSPORT_MODE = TransportMode.OJEK;
    private static final double SAMPLE_DISTANCE = 6.7;
    private static final String SAMPLE_PICKUP_ADDRESS = "North Jakarta";
    private static final String SAMPLE_DEST_ADDRESS = "Central Jakarta";
    private static final String CONFIRM_MSG = "iya";
    private static final String REINPUT_MSG = "ulang";
    private static final String INVALID_MSG = "Hello World!";
    private static final String SAMPLE_REINPUT_REPLY = "Hello World!";
    private static final String END_ORDER_REPLY = "Terima kasih atas pesanan anda.";

    private static final String INVALID_REPLY = "Perintah anda tidak dikenal!\n"
        + "Ketik: \n"
        + "-) '!transport iya' untuk konfirmasi pesanan anda\n"
        + "-) '!transport ulang' untuk input ulang alamat pickup dan tujuan anda.";

    private static final String OPENING_REPLY = "Anda akan memesan layanan "
        + TRANSPORT_MODE.getAlias()
        + " untuk tujuan dari:\n\n"
        + SAMPLE_PICKUP_ADDRESS + "\n\n"
        + "menuju\n\n"
        + SAMPLE_DEST_ADDRESS + "\n\n"
        + "Untuk biaya sebesar Rp "
        + NumberFormat.getNumberInstance(Locale.US)
            .format(Math.round(SAMPLE_DISTANCE) * TRANSPORT_MODE
                .getCostPerKm()) + ".00\n"
        + "dengan jarak berkendara " + SAMPLE_DISTANCE + " km\n\n"
        + "Apakah anda konfirmasi pemesanan ini?\n"
        + "Ketik:\n"
        + "-) '!transport iya' untuk konfirmasi\n"
        + "-) '!transport ulang' untuk mengubah alamat pickup dan tujuan.";

    private static final String ERROR_REPLY = "Sedang ada masalah dalam perhitungan"
        + " jarak, mohon menginput ulang"
        + " mulai dari alamat pickup anda.";

    private static final String INVALID_ADDRESS_REPLY = "Alamat / rute tidak ditemukan!\n"
        + "Mohon memasukkan ulang alamat mulai dari alamat pickup anda.";

    private static final String DRIVER_NAME = "Mock Driver";
    private static final String DRIVER_LICENSE = "B1234A";

    private static final String DRIVER_CREDS = "Nama driver: "
        + DRIVER_NAME
        + "\n"
        + "Nomor polisi kendaraan: " + DRIVER_LICENSE;

    private static final String NO_DRIVER_REPLY = "Mohon maaf, saat ini kami tidak menemukan "
        + "pengemudi untuk tipe kendaraan yang anda telah pilih. Mohon memesan kembali di lain "
        + "waktu dengan perintah '!transport'";

    /**
     * Sets up a TrasnportUser instance in Confirm Order state.
     */
    @BeforeEach
    public void setUp() {
        transportUser.setPickupAddress(SAMPLE_PICKUP_ADDRESS);
        transportUser.setDestinationAddress(SAMPLE_DEST_ADDRESS);
        transportUser.setTransportMode(TRANSPORT_MODE);
        transportUser.setCurrentState(confirmOrderState);

        confirmOrderState.setReinputState(mockReinputState);
        confirmOrderState.setFinishState(mockArbitraryState);

        driver = new Driver();
        driver.setName(DRIVER_NAME);
        driver.setPlateNumber(DRIVER_LICENSE);
    }

    @Test
    void testConfirmOrderMessage() {
        when(driverNotifier.notifyOneDriverForUser(transportUser)).thenReturn(driver);
        String response = confirmOrderState.handleMessage(CONFIRM_MSG);
        Assertions.assertNotEquals(transportUser.getCurrentState(), confirmOrderState);
        Assertions.assertEquals(END_ORDER_REPLY + "\n\n" + DRIVER_CREDS, response);
    }

    @Test
    void testConfirmOrderMessageButNoDriver() {
        lenient().when(driverNotifier.notifyOneDriverForUser(transportUser)).thenReturn(null);
        String response = confirmOrderState.handleMessage(CONFIRM_MSG);
        Assertions.assertNotEquals(transportUser.getCurrentState(), confirmOrderState);
        Assertions.assertEquals(NO_DRIVER_REPLY, response);

    }

    @Test
    void testAskForReinputMessage() {
        when(mockReinputState.getOpeningMessage()).thenReturn(SAMPLE_REINPUT_REPLY);

        String response = confirmOrderState.handleMessage(REINPUT_MSG);
        Assertions.assertEquals(transportUser.getCurrentState(), mockReinputState);
        Assertions.assertEquals(SAMPLE_REINPUT_REPLY, response);
    }

    @Test
    void testInvalidMessageReply() {
        String response = confirmOrderState.handleMessage(INVALID_MSG);
        Assertions.assertEquals(INVALID_REPLY, response);
    }

    @Test
    void testOpeningMessageShouldHoldValidData() throws Exception {
        when(mapUtil.getDrivingDistance(transportUser
            .getPickupAddress(), transportUser.getDestinationAddress()))
            .thenReturn(SAMPLE_DISTANCE);
        Assertions.assertEquals(OPENING_REPLY, confirmOrderState.getOpeningMessage());
    }

    @Test
    void testOpeningMessageErrorConnection() throws Exception {
        when(mapUtil.getDrivingDistance(transportUser
        .getPickupAddress(), transportUser.getDestinationAddress()))
            .thenThrow(new IOException());

        String response = confirmOrderState.getOpeningMessage();

        Assertions.assertEquals(transportUser.getCurrentState(), mockReinputState);
        Assertions.assertEquals(ERROR_REPLY, response);
    }

    @Test
    void testInvalidAddressInput() throws Exception {
        when(mapUtil.getDrivingDistance(transportUser
            .getPickupAddress(), transportUser.getDestinationAddress()))
            .thenThrow(new AddressNotFoundException());

        String response = confirmOrderState.getOpeningMessage();

        Assertions.assertEquals(transportUser.getCurrentState(), mockReinputState);
        Assertions.assertEquals(INVALID_ADDRESS_REPLY, response);
    }
}
