package csui.serbagunabot.transportasi.map;

import csui.serbagunabot.transportasi.map.exception.AddressNotFoundException;
import java.io.IOException;

public interface MapUtil {

    public double getDrivingDistance(String pickupLocation, String destinationLocation) throws
        IOException, AddressNotFoundException;
}
