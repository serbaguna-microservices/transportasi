package csui.serbagunabot.transportasi.chat.generator;

import csui.serbagunabot.transportasi.chat.states.DriverState;
import csui.serbagunabot.transportasi.chat.states.driver.DriverInputNameState;
import csui.serbagunabot.transportasi.chat.states.driver.DriverInputVehicleTypeState;
import csui.serbagunabot.transportasi.chat.states.driver.DriverLicensePlateState;
import csui.serbagunabot.transportasi.chat.states.driver.DriverStartState;
import csui.serbagunabot.transportasi.model.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;

@Component
public class DriverStateGenerator {

    @Autowired
    private AutowireCapableBeanFactory autowireCapableBeanFactory;

    /**
     * Driver state for registration.
     */
    public DriverState generateRegisterState(Driver driver) {
        DriverStartState driverStartState = new DriverStartState(driver);
        DriverInputNameState driverInputNameState = new DriverInputNameState(driver);
        DriverInputVehicleTypeState vehicleTypeState = new DriverInputVehicleTypeState(driver);
        DriverLicensePlateState driverLicensePlateState = new DriverLicensePlateState(driver);

        driverStartState.setNextState(driverInputNameState);
        driverInputNameState.setNextState(vehicleTypeState);
        vehicleTypeState.setNextState(driverLicensePlateState);

        autowireCapableBeanFactory
            .autowireBeanProperties(driverLicensePlateState,
                AutowireCapableBeanFactory.AUTOWIRE_AUTODETECT,
                true);

        return driverStartState;
    }


}
