package csui.serbagunabot.transportasi.chat.states;

import csui.serbagunabot.transportasi.model.TransportUser;

public abstract class TransportState {

    protected TransportUser transportUser;

    protected TransportState(TransportUser transportUser) {
        this.transportUser = transportUser;
    }

    /**
     * Handles message and moves to state or not based on the message.
     * @param message input from the user
     * @return reply message string
     */
    public abstract String handleMessage(String message);

    /**
     * Message sent to user when entering this state.
     * @return message to reply
     */
    public abstract String getOpeningMessage();
}
