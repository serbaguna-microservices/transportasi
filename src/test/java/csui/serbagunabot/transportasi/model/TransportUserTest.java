package csui.serbagunabot.transportasi.model;

import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import csui.serbagunabot.transportasi.chat.states.common.NoTransportState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TransportUserTest {

    @Mock
    NoTransportState noTransportStateMock;

    private TransportUser transportUser;
    private static final String MESSAGE_SAMPLE = "Hello World!";
    private static final String ADDRESS_SAMPLE = "Earth";

    @BeforeEach
    public void setUp() {
        transportUser = new TransportUser();
    }

    @Test
    void testRespondShouldCallStateRespond() {
        transportUser.setCurrentState(noTransportStateMock);

        transportUser.respond(MESSAGE_SAMPLE);
        verify(noTransportStateMock, times(1))
                .handleMessage(MESSAGE_SAMPLE);

    }

    @Test
    void testGetterSetterPickupAddress() {
        transportUser.setPickupAddress(ADDRESS_SAMPLE);
        Assertions.assertEquals(ADDRESS_SAMPLE, transportUser.getPickupAddress());
    }

    @Test
    void testGetterSetterDestinationAddress() {
        transportUser.setDestinationAddress(ADDRESS_SAMPLE);
        Assertions.assertEquals(ADDRESS_SAMPLE, transportUser.getDestinationAddress());
    }

}
