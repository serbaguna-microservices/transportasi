package csui.serbagunabot.transportasi.model;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransportUser {

    private TransportState currentState;
    private TransportMode transportMode;
    private String pickupAddress;
    private String destinationAddress;

    public String respond(String message) {
        return this.currentState.handleMessage(message);
    }

}
