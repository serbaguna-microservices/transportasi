package csui.serbagunabot.transportasi.controller;

import csui.serbagunabot.transportasi.model.line.LineRequest;
import csui.serbagunabot.transportasi.model.line.LineResponse;
import csui.serbagunabot.transportasi.service.TransportasiHandlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransportasiController {

    @Autowired
    TransportasiHandlerService transportasiHandlerService;

    /**
     * Called by the proxy server if transportasi message was received.
     * @param lineRequest Instance containing message and userId
     */
    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<LineResponse> replyEntrypoint(@RequestBody LineRequest lineRequest) {
        String response = transportasiHandlerService
            .handleMessage(lineRequest.getMessage(), lineRequest.getUserId());

        return ResponseEntity.ok(new LineResponse(response));
    }


}
