package csui.serbagunabot.transportasi.chat.generator;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.chat.states.common.*;
import csui.serbagunabot.transportasi.chat.states.helikopter.HelicopterConfirmState;
import csui.serbagunabot.transportasi.chat.states.helikopter.HelicopterDestinationState;
import csui.serbagunabot.transportasi.chat.states.ojek.HealthProtocolState;
import csui.serbagunabot.transportasi.chat.states.ojek.OjekPandemicDestinationState;
import csui.serbagunabot.transportasi.chat.states.taxi.AskTaxiModeState;
import csui.serbagunabot.transportasi.chat.states.taxi.ConfirmTaxiOrderState;
import csui.serbagunabot.transportasi.model.TransportUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;

@Component
public class TransportStateGenerator {

    @Autowired
    private AutowireCapableBeanFactory autowireCapableBeanFactory;

    private static final String OJEK_CLOSING_MSG = "Terima kasih atas pesanan ojek anda.\n"
        + "Driver akan segera datang ke lokasi untuk menjemput anda.";

    private static final String HELI_CLOSING_MSG = "Terima kasih atas pesanan helikopter anda.\n"
        + "Pilot akan segera tiba di lokasi penjemputan anda\n"
        + "Mohon untuk selalu berjaga jarak saat helikopter melakukan landing atau take-off.";

    private static final String TAXI_CLOSING_MSG = "Terima kasih atas pesanan taksi anda.\n"
        + "Driver akan segera datang ke lokasi anda, jangan lupa menerapkan protokol kesehatan.";


    /**
     * Generates initial TransportState for users who had just sent !transport.
     * No State -> Ask method of transport.
     */
    public TransportState generateStartingStates(TransportUser user) {
        NoTransportState noState = new NoTransportState(user);
        AskTransportChoiceState askChoiceState = new AskTransportChoiceState(user);

        noState.setNextState(askChoiceState);

        autowireCapableBeanFactory
            .autowireBeanProperties(askChoiceState,
                AutowireCapableBeanFactory.AUTOWIRE_AUTODETECT,
                true);

        return  noState;
    }

    /**
     * Create chain of ojek service states after !transport ojek.
     * Health Proc -> Pickup -> Destination -> Confirm Order.
     */
    public TransportState generateOjekStates(TransportUser user) {
        HealthProtocolState healthProtocolState = new HealthProtocolState(user);
        PickupLocationState pickupLocationState = new PickupLocationState(user);
        OjekPandemicDestinationState destinationState = new OjekPandemicDestinationState(user);
        ConfirmOrderState confirmOrderState = new ConfirmOrderState(user);

        healthProtocolState.setAgreeState(pickupLocationState);
        pickupLocationState.setNextState(destinationState);
        destinationState.setNextState(confirmOrderState);
        destinationState.setLocationInvalidState(pickupLocationState);
        confirmOrderState.setReinputState(pickupLocationState);

        confirmOrderState.setClosingMessage(OJEK_CLOSING_MSG);

        TransportState startingChain = generateStartingStates(user);
        confirmOrderState.setFinishState(startingChain);
        healthProtocolState.setDeclineState(startingChain);

        autowireCapableBeanFactory
            .autowireBeanProperties(destinationState,
                AutowireCapableBeanFactory.AUTOWIRE_AUTODETECT,
                true);

        autowireCapableBeanFactory
            .autowireBeanProperties(confirmOrderState,
                AutowireCapableBeanFactory.AUTOWIRE_AUTODETECT,
                true);

        return healthProtocolState;
    }

    /**
     * Create chain of helicopter service states after !transport heli.
     * Rules Confirm -> Pickup -> Destination -> Confirm Order.
     */
    public TransportState generateHeliStates(TransportUser transportUser) {
        HelicopterConfirmState heliConfirmState = new HelicopterConfirmState(transportUser);
        PickupLocationState pickupLocationState = new PickupLocationState(transportUser);
        HelicopterDestinationState heliDestinationState =
            new HelicopterDestinationState(transportUser);
        ConfirmOrderState confirmOrderState = new ConfirmOrderState(transportUser);

        confirmOrderState.setReinputState(pickupLocationState);
        heliDestinationState.setNextState(confirmOrderState);
        heliConfirmState.setAcceptState(pickupLocationState);
        pickupLocationState.setNextState(heliDestinationState);
        confirmOrderState.setClosingMessage(HELI_CLOSING_MSG);

        TransportState startingChain = generateStartingStates(transportUser);
        heliConfirmState.setRejectState(startingChain);
        confirmOrderState.setFinishState(startingChain);

        autowireCapableBeanFactory
            .autowireBeanProperties(confirmOrderState,
                AutowireCapableBeanFactory.AUTOWIRE_AUTODETECT,
                true);

        return heliConfirmState;
    }

    /**
     * Create chain of taxi states after !transport taxi.
     */
    public TransportState generateTaxiStates(TransportUser transportUser) {
        AskTaxiModeState askTaxiModeState = new AskTaxiModeState(transportUser);
        PickupLocationState pickupLocationState = new PickupLocationState(transportUser);
        DestinationLocationState destinationLocationState =
            new DestinationLocationState(transportUser);
        ConfirmTaxiOrderState confirmTaxiState = new ConfirmTaxiOrderState(transportUser);

        askTaxiModeState.setNextState(pickupLocationState);
        pickupLocationState.setNextState(destinationLocationState);
        destinationLocationState.setNextState(confirmTaxiState);
        confirmTaxiState.setReinputState(pickupLocationState);
        confirmTaxiState.setClosingMessage(TAXI_CLOSING_MSG);

        autowireCapableBeanFactory
            .autowireBeanProperties(confirmTaxiState,
                AutowireCapableBeanFactory.AUTOWIRE_AUTODETECT,
                true);

        return askTaxiModeState;
    }

}
