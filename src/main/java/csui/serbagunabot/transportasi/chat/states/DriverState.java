package csui.serbagunabot.transportasi.chat.states;

import csui.serbagunabot.transportasi.model.Driver;

public abstract class DriverState {

    protected Driver driver;

    protected DriverState(Driver driver) {
        this.driver = driver;
    }

    /**
     * Handles message and moves to state or not based on the message.
     * @param message input from the user
     * @return reply message string
     */
    public abstract String handleMessage(String message);

    /**
     * Message sent to user when entering this state.
     * @return message to reply
     */
    public abstract String getOpeningMessage();


}
