package csui.serbagunabot.transportasi.feign;

import csui.serbagunabot.transportasi.model.line.LineRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "bookrs", url = "${serbaguna.push_url}")
public interface LinePushFeign {

    @PostMapping("/")
    void sendPushMessage(LineRequest lineRequest);

}
