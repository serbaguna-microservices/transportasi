package csui.serbagunabot.transportasi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TransportMode {
    OJEK(2500, "Ojek"),
    TAXI_LARGE(10000, "Taksi Large"),
    TAXI_STANDARD(8000, "Taksi Standard"),
    HELICOPTER(750000, "Helikopter");

    private long costPerKm;
    private String alias;
}
