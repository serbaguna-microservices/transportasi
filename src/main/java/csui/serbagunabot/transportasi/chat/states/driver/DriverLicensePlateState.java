package csui.serbagunabot.transportasi.chat.states.driver;

import csui.serbagunabot.transportasi.chat.states.DriverState;
import csui.serbagunabot.transportasi.model.Driver;
import csui.serbagunabot.transportasi.repository.DriverRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class DriverLicensePlateState extends DriverState {

    @Autowired
    DriverRepository driverRepository;

    public DriverLicensePlateState(Driver driver) {
        super(driver);
    }

    @Override
    public String handleMessage(String message) {
        driver.setPlateNumber(message);
        driverRepository.save(driver);
        return getClosingMessage();
    }

    @Override
    public String getOpeningMessage() {
        return "Silahkan masukan nomor polisi dari kendaraan anda.\n"
            + "Contoh: '!driver BG1234JE";
    }

    private String getClosingMessage() {
        return "Terima kasih atas pendaftaran anda.\n"
            + "Anda terdaftar dengan nama " + driver.getName() + "\n"
            + "Untuk melayani jasa transportasi dengan kendaraan " + driver.getVehicleType()
            + "\n"
            + "dengan nomor polisi "
            + driver.getPlateNumber();

    }
}
