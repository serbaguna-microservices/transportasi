package csui.serbagunabot.transportasi.map.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class BingResourceSet {
    private List<BingResource> resources;
}
