package csui.serbagunabot.transportasi.controller;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.serbagunabot.transportasi.model.line.LineRequest;
import csui.serbagunabot.transportasi.service.TransportasiHandlerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class TransportasiControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    TransportasiHandlerService transportasiHandlerService;

    private LineRequest lineRequest;

    public static final String SAMPLE_RESPONSE = "Hello World!";
    public static final String SAMPLE_USERID = "ABCDE";
    public static final String SAMPLE_MSG = "Hi";

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }


    @BeforeEach
    void setUp() {
        lineRequest = new LineRequest();
        lineRequest.setMessage(SAMPLE_MSG);
        lineRequest.setUserId(SAMPLE_USERID);
    }

    @Test
    void testReplyEntryPointTransportasiShouldReturnJson() throws Exception {
        when(transportasiHandlerService
            .handleMessage(anyString(), anyString()))
            .thenReturn(SAMPLE_RESPONSE);

        mvc.perform(post("/")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(mapToJson(lineRequest)))
            .andExpect(jsonPath("$.response")
                .value(SAMPLE_RESPONSE));
    }

}
