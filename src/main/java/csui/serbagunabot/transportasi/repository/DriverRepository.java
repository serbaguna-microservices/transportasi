package csui.serbagunabot.transportasi.repository;

import csui.serbagunabot.transportasi.model.Driver;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DriverRepository extends JpaRepository<Driver, String> {

    List<Driver> findAllByVehicleType(String vehicleType);

}
