package csui.serbagunabot.transportasi.model.line;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class LineResponse {
    private String response;
}
