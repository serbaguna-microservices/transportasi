package csui.serbagunabot.transportasi.chat.states.common;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportUser;
import lombok.Setter;

public class PickupLocationState extends TransportState {

    @Setter
    private TransportState nextState;

    public PickupLocationState(TransportUser transportUser) {
        super(transportUser);
    }

    @Override
    public String handleMessage(String message) {
        // Only need to save the message that is the address
        transportUser.setPickupAddress(message);
        transportUser.setCurrentState(nextState);
        return nextState.getOpeningMessage();
    }

    @Override
    public String getOpeningMessage() {
        return "Silahkan masukkan alamat tempat "
            + "anda ingin di pickup sekarang.\n\n"
            + "Contoh jawaban: \n"
            + "'!transport Stasiun Universitas Indonesia, Pondok Cina, Depok' \n";
    }
}
