package csui.serbagunabot.transportasi.chat.states.driver;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import csui.serbagunabot.transportasi.model.Driver;
import csui.serbagunabot.transportasi.model.TransportMode;
import csui.serbagunabot.transportasi.repository.DriverRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DriverLicensePlateStateTest {

    private Driver driver = new Driver();

    @Mock
    DriverRepository driverRepository;

    @InjectMocks
    DriverLicensePlateState driverLicensePlateState = new DriverLicensePlateState(driver);

    private static final String LICENSE_NO = "123456";
    private static final String VEHICLE_TYPE = TransportMode.HELICOPTER.getAlias();
    private static final String DRIVER_NAME = "ABC XYZ";

    private static final String CLOSING_REPLY = "Terima kasih atas pendaftaran anda.\n"
        + "Anda terdaftar dengan nama " + DRIVER_NAME + "\n"
        + "Untuk melayani jasa transportasi dengan kendaraan " + VEHICLE_TYPE
        + "\n"
        + "dengan nomor polisi "
        + LICENSE_NO;

    private static final String OPENING_REPLY = "Silahkan masukan "
        + "nomor polisi dari kendaraan anda.\n"
        + "Contoh: '!driver BG1234JE";

    @BeforeEach
    void setUp() {
        driver.setCurrentState(driverLicensePlateState);
        driver.setVehicleType(VEHICLE_TYPE);
        driver.setPlateNumber(LICENSE_NO);
        driver.setName(DRIVER_NAME);
    }

    @Test
    void testMessageSetLicenseNumber() {
        String response = driverLicensePlateState.handleMessage(LICENSE_NO);
        Assertions.assertEquals(LICENSE_NO, driver.getPlateNumber());
        verify(driverRepository, times(1)).save(driver);

        Assertions.assertEquals(CLOSING_REPLY, response);
    }

    @Test
    void testOpeningMessage() {
        String response = driverLicensePlateState.getOpeningMessage();
        Assertions.assertEquals(OPENING_REPLY, response);
    }



}
