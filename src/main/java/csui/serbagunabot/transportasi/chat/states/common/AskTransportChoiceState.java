package csui.serbagunabot.transportasi.chat.states.common;

import csui.serbagunabot.transportasi.chat.generator.TransportStateGenerator;
import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportMode;
import csui.serbagunabot.transportasi.model.TransportUser;
import org.springframework.beans.factory.annotation.Autowired;

public class AskTransportChoiceState extends TransportState {

    @Autowired
    TransportStateGenerator transportStateGenerator;

    private static final String CHOICE_HELP_MSG = "Bantuan layanan pemesanan transportasi:\n"
            + "-) Ketik '!transport ojek' untuk memesan ojek\n"
            + "-) Ketik '!transport taxi' untuk memesan mobil taksi\n"
            + "-) Ketik '!transport heli' untuk memesan helikopter";

    private static final String NOT_FOUND_MSG = "Pilihan anda tidak dikenal, silahkan ketik "
            + "'!transport help' untuk menunjukkan kembali pilihan";

    public AskTransportChoiceState(TransportUser transportUser) {
        super(transportUser);
    }

    @Override
    public String handleMessage(String message) {
        // Sets state to each transport modes based on choices
        switch (message) {
            case "help":
                return CHOICE_HELP_MSG;
            case "ojek":
                transportUser.setTransportMode(TransportMode.OJEK);
                transportUser.setCurrentState(transportStateGenerator
                    .generateOjekStates(transportUser));
                return transportUser.getCurrentState().getOpeningMessage();
            case "taxi":
                transportUser.setCurrentState(transportStateGenerator
                    .generateTaxiStates(transportUser));
                return transportUser.getCurrentState().getOpeningMessage();
            case "heli":
                transportUser.setTransportMode(TransportMode.HELICOPTER);
                transportUser.setCurrentState(transportStateGenerator
                    .generateHeliStates(transportUser));
                return transportUser.getCurrentState().getOpeningMessage();
            default:
                return NOT_FOUND_MSG;
        }
    }

    @Override
    public String getOpeningMessage() {
        return "Selamat datang di fitur pemesanan transportasi!\n"
            + "Anda bisa memacam salah satu dari tiga layanan transportasi kami sebagai berikut\n"
            + "Ketik:\n"
            + "-) '!transport ojek' untuk memesan ojek\n"
            + "-) '!transport taxi' untuk memesan mobil taksi\n"
            + "-) '!transport heli' untuk memesan helikopter";
    }
}
