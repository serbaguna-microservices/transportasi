package csui.serbagunabot.transportasi.chat.states.common;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportUser;
import lombok.Setter;

public class DestinationLocationState extends TransportState {

    @Setter
    private TransportState nextState;

    public DestinationLocationState(TransportUser transportUser) {
        super(transportUser);
    }

    @Override
    public String handleMessage(String message) {
        transportUser.setDestinationAddress(message);
        transportUser.setCurrentState(nextState);
        return nextState.getOpeningMessage();
    }

    @Override
    public String getOpeningMessage() {
        return "Silahkan masukkan alamat tujuan bepergian anda dengan contoh:\n"
            + "'!transport Stasiun Universitas Indonesia, Pondok Cina, Depok'";
    }
}
