package csui.serbagunabot.transportasi.chat.states.ojek;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportUser;
import lombok.Setter;

public class HealthProtocolState extends TransportState {

    private static final String INVALID_MESSAGE = "Jawaban tidak dikenal, mohon menjawab dengan\n"
        + "'!transport iya' untuk setuju dengan protokol kesehatan\n"
        + "atau '!transport tidak' untuk tidak setuju dengan protokol kesehatan.";

    private static final String DECLINE_MESSAGE = "Mohon maaf, untuk menggunakan layanan ojek, "
        + "anda wajib setuju dan mematuhi protokol kesehatan yang sudah tertera.\n\n"
        + "Silahkan melakukan pesanan kembali dengan perintah '!transport'";

    @Setter
    private TransportState agreeState;

    @Setter
    private TransportState declineState;

    public HealthProtocolState(TransportUser transportUser) {
        super(transportUser);
    }

    @Override
    public String handleMessage(String message) {
        switch (message) {
            case "iya":
                // Moves on to state set as agree
                transportUser.setCurrentState(agreeState);
                return agreeState.getOpeningMessage();
            case "tidak":
                // Regenerate and goes back to original no state
                transportUser.setCurrentState(declineState);
                return DECLINE_MESSAGE;
            default:
                return INVALID_MESSAGE;
        }
    }

    @Override
    public String getOpeningMessage() {
        return "Perhatian! dikarenakan pandemi COVID-19, "
            + "Semua pelanggan transportasi ojek harus mengikuti "
            + "protokol kesehatan sebagai berikut:\n"
            + "1) Helm harus disediakan oleh pelanggan masing-masing.\n"
            + "2) Wajib menggunakan masker selama perjalanan.\n"
            + "3) Tutup mulut anda dengan siku atau bahu saat bersin atau batuk.\n"
            + "4) Cuci tangan setidaknya selama 20 detik setelah berkendara.\n"
            + "5) Jarak berkendara antar lokasi wajib <= 8 km\n"
            + "Apabila anda setuju dengan protokol kesehatan maka\n\n"
            + "Ketik:\n"
            + "-) '!transport iya' untuk setuju\n"
            + "-) '!transport tidak' untuk tidak setuju";
    }
}
