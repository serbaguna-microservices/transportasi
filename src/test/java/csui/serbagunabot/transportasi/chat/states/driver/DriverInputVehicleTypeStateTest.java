package csui.serbagunabot.transportasi.chat.states.driver;

import csui.serbagunabot.transportasi.chat.states.DriverState;
import csui.serbagunabot.transportasi.model.Driver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DriverInputVehicleTypeStateTest {

    private Driver driver;

    @Mock
    DriverState mockState;

    @InjectMocks
    DriverInputVehicleTypeState driverInputVehicleTypeState;

    private static final String LIST_CMD = "list";
    private static final String INVALID_CMD = "XYZ";

    private static final String VEHICLE_LIST = "1) Motor (Ojek)\n"
        + "2) Mobil Standard (Taksi Standard 2-3 orang)\n"
        + "3) Mobil Besar (Taksi Large 4-5 orang)\n"
        + "4) Helikopter\n";

    private static final String INVALID_CHOICE_MSG = "Pilihan tidak valid!\n"
        + "Ketik '!driver n' dengan n adalah pilihan anda\n"
        + "atau '!driver list' untuk list ulang pilhian.";

    private static final String OPENING_MSG = "Silahkan pilih jenis transportasi yang "
        + "anda sediakan jasanya untuk.\n"
        + VEHICLE_LIST
        + "Ketik '!driver n' dengan n nomor pilihan anda.";

    @BeforeEach
    void setUp() {
        driver = new Driver();
        driverInputVehicleTypeState = new DriverInputVehicleTypeState(driver);
        driverInputVehicleTypeState.setNextState(mockState);
    }

    @ParameterizedTest
    @CsvSource({
        "'1', 'Ojek'",
        "'2', 'Taksi Standard'",
        "'3', 'Taksi Large'",
        "'4', 'Helikopter'",
    })
    void testHandleMessageShouldChangeVehicleType(String code, String expectedType) {
        driverInputVehicleTypeState.handleMessage(code);
        Assertions.assertEquals(expectedType, driver.getVehicleType());
        Assertions.assertEquals(mockState, driver.getCurrentState());
    }

    @Test
    void testListChoicesAgain() {
        String response = driverInputVehicleTypeState.handleMessage(LIST_CMD);
        Assertions.assertEquals(VEHICLE_LIST, response);
    }

    @Test
    void testInvalidMessage() {
        String response = driverInputVehicleTypeState.handleMessage(INVALID_CMD);
        Assertions.assertEquals(INVALID_CHOICE_MSG, response);
    }

    @Test
    void testGetOpeningMessage() {
        String response = driverInputVehicleTypeState.getOpeningMessage();
        Assertions.assertEquals(OPENING_MSG, response);
    }



}
