package csui.serbagunabot.transportasi.chat.states.common;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportUser;
import lombok.Setter;

public class NoTransportState extends TransportState {

    private static final String NOT_FOUND_MSG = "Perintah tidak dikenal!\n"
            + "Ketik !transport untuk mulai menggunakan fitur transportasi";

    @Setter
    private TransportState nextState;

    public NoTransportState(TransportUser transportUser) {
        super(transportUser);
    }

    @Override
    public String handleMessage(String message) {
        if (message.isEmpty()) {
            transportUser.setCurrentState(nextState);
            return nextState.getOpeningMessage();
        }
        return NOT_FOUND_MSG;
    }

    @Override
    public String getOpeningMessage() {
        return "Selamat datang di fitur pemesanan transportasi";
    }
}
