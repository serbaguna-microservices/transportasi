package csui.serbagunabot.transportasi.chat.generator;

import csui.serbagunabot.transportasi.chat.states.DriverState;
import csui.serbagunabot.transportasi.chat.states.driver.DriverStartState;
import csui.serbagunabot.transportasi.model.Driver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

@ExtendWith(MockitoExtension.class)
class DriverStateGeneratorTest {

    private Driver driver;

    @Mock
    AutowireCapableBeanFactory autowireCapableBeanFactory;

    @InjectMocks
    DriverStateGenerator driverStateGenerator;

    @BeforeEach
    void setUp() {
        driver = new Driver();
    }

    @Test
    void testGenerateRegisterState() {
        DriverState driverState = driverStateGenerator.generateRegisterState(driver);
        driverStateGenerator.generateRegisterState(driver);

        Assertions.assertTrue(driverState instanceof DriverStartState);
    }

}
