package csui.serbagunabot.transportasi.chat.states.helikopter;

import static org.mockito.Mockito.*;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HelicopterConfirmStateTest {

    @Mock
    TransportState mockAgreeState;

    @Mock
    TransportState mockDeclineState;

    HelicopterConfirmState helicopterConfirmState;

    TransportUser transportUser;

    private static final String AGREE_MSG = "iya";
    private static final String DECLINE_MSG = "tidak";
    private static final String SAMPLE_REPLY = "Hello World!";
    private static final String INVALID_MSG = "abc";
    private static final String REJECT_MESSAGE = "Pemesanan helikopter telah dibatalkan.\n"
        + "Silahkan melakukan pemesanan transportasi dengan perintah '!transport' kembali.";
    private static final String HELP_REPLY = "Perintah tidak dikenal!\n"
        + "Ketik '!transport iya' untuk konfirmasi ketersediaan layanan atau\n"
        + "Ketik '!transport tidak' untuk batal pemesanan helikopter.";
    private static final String OPENING_REPLY = "Perhatian! Pemesanan helikopter baru"
        + " hanya tersedia di daerah berikut:\n"
        + "1) DKI Jakarta dan Tanggerang\n"
        + "2) Depok\n"
        + "Serta lokasi pickup anda harus berada di Helipad atau ruang terbuka yang luas\n\n"
        + "Tujuan dari pemesanan helikopter ini juga hanya terbatas ke bandara tertentu"
        + "di daerah Jakarta, Tanggeran, dan Depok\n\n"
        + "Apakah anda konfirmasi dengan ketentuan berikut?\n"
        + "Ketik:\n"
        + "'!transport iya' untuk konfirmasi\n"
        + "'!transport tidak' untuk batal pemesanan helikopter";

    @BeforeEach
    void setUp() {
        transportUser = new TransportUser();
        helicopterConfirmState = new HelicopterConfirmState(transportUser);
        helicopterConfirmState.setAcceptState(mockAgreeState);
        helicopterConfirmState.setRejectState(mockDeclineState);

        transportUser.setCurrentState(helicopterConfirmState);
    }

    @Test
    void testAgreeMessageHandle() {
        when(mockAgreeState.getOpeningMessage()).thenReturn(SAMPLE_REPLY);
        String response = helicopterConfirmState.handleMessage(AGREE_MSG);

        verify(mockAgreeState, times(1)).getOpeningMessage();
        Assertions.assertEquals(SAMPLE_REPLY, response);
    }

    @Test
    void testDeclineMessageHandle() {
        String response = helicopterConfirmState.handleMessage(DECLINE_MSG);

        Assertions.assertEquals(mockDeclineState, transportUser.getCurrentState());
        Assertions.assertEquals(REJECT_MESSAGE, response);
    }

    @Test
    void testInvalidMessageHandle() {
        String response = helicopterConfirmState.handleMessage(INVALID_MSG);
        Assertions.assertEquals(HELP_REPLY, response);
    }

    @Test
    void testOpeningMessage() {
        String response = helicopterConfirmState.getOpeningMessage();
        Assertions.assertEquals(OPENING_REPLY, response);
    }

}
