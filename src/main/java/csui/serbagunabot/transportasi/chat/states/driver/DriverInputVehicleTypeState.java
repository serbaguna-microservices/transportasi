package csui.serbagunabot.transportasi.chat.states.driver;

import csui.serbagunabot.transportasi.chat.states.DriverState;
import csui.serbagunabot.transportasi.model.Driver;
import csui.serbagunabot.transportasi.model.TransportMode;
import lombok.Setter;

public class DriverInputVehicleTypeState extends DriverState {

    @Setter
    private DriverState nextState;

    private static final String INVALID_CHOICE_MSG = "Pilihan tidak valid!\n"
        + "Ketik '!driver n' dengan n adalah pilihan anda\n"
        + "atau '!driver list' untuk list ulang pilhian.";

    private static final String VEHICLE_LIST = "1) Motor (Ojek)\n"
        + "2) Mobil Standard (Taksi Standard 2-3 orang)\n"
        + "3) Mobil Besar (Taksi Large 4-5 orang)\n"
        + "4) Helikopter\n";

    public DriverInputVehicleTypeState(Driver driver) {
        super(driver);
    }

    @Override
    public String handleMessage(String message) {
        switch (message) {
            case "1":
                driver.setVehicleType(TransportMode.OJEK.getAlias());
                break;
            case "2":
                driver.setVehicleType(TransportMode.TAXI_STANDARD.getAlias());
                break;
            case "3":
                driver.setVehicleType(TransportMode.TAXI_LARGE.getAlias());
                break;
            case "4":
                driver.setVehicleType(TransportMode.HELICOPTER.getAlias());
                break;
            case "list":
                return VEHICLE_LIST;
            default:
                return INVALID_CHOICE_MSG;
        }
        driver.setCurrentState(nextState);
        return nextState.getOpeningMessage();
    }

    @Override
    public String getOpeningMessage() {
        return "Silahkan pilih jenis transportasi yang anda sediakan jasanya untuk.\n"
            + VEHICLE_LIST
            + "Ketik '!driver n' dengan n nomor pilihan anda.";
    }
}
