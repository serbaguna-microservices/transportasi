package csui.serbagunabot.transportasi.model;

import csui.serbagunabot.transportasi.chat.states.DriverState;
import javax.persistence.*;
import lombok.*;

@Entity
@Table(name = "Driver")
@NoArgsConstructor
@Setter
@Getter
public class Driver {

    @Transient
    private DriverState currentState;

    @Id
    @Column(name = "lineUserId", updatable = false)
    private String lineUserId;

    @Column(name = "name")
    private String name;

    @Column(name = "plateNumber")
    private String plateNumber;

    @Column(name = "vehicleType")
    private String vehicleType;

    public String respond(String message) {
        return this.currentState.handleMessage(message);
    }

}
