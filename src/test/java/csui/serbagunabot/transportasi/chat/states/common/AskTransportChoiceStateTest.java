package csui.serbagunabot.transportasi.chat.states.common;

import static org.mockito.Mockito.when;

import csui.serbagunabot.transportasi.chat.generator.TransportStateGenerator;
import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportMode;
import csui.serbagunabot.transportasi.model.TransportUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AskTransportChoiceStateTest {

    @Mock
    TransportState arbitraryState;

    @Mock
    TransportStateGenerator transportStateGenerator;

    TransportUser transportUser = new TransportUser();

    @InjectMocks
    AskTransportChoiceState askChoiceState = new AskTransportChoiceState(transportUser);

    private static final String HELP_MSG = "help";
    private static final String OJEK_MSG = "ojek";
    private static final String TAXI_MSG = "taxi";
    private static final String HELI_MSG = "heli";
    private static final String INVALID_MSG = "Hello World!";
    private static final String SAMPLE_REPLY = "Hello World!";

    private static final String TRANSPORT_CHOICE_REPLY = "Selamat datang di fitur "
        + "pemesanan transportasi!\n"
        + "Anda bisa memacam salah satu dari tiga layanan transportasi kami sebagai berikut\n"
        + "Ketik:\n"
        + "-) '!transport ojek' untuk memesan ojek\n"
        + "-) '!transport taxi' untuk memesan mobil taksi\n"
        + "-) '!transport heli' untuk memesan helikopter";

    private static final String CHOICE_HELP_REPLY = "Bantuan layanan pemesanan transportasi:\n"
            + "-) Ketik '!transport ojek' untuk memesan ojek\n"
            + "-) Ketik '!transport taxi' untuk memesan mobil taksi\n"
            + "-) Ketik '!transport heli' untuk memesan helikopter";

    private static final String NOT_FOUND_REPLY = "Pilihan anda tidak dikenal, silahkan ketik "
            + "'!transport help' untuk menunjukkan kembali pilihan";

    @BeforeEach
    public void setUp() {
        transportUser.setCurrentState(askChoiceState);
    }

    @Test
    void testHandleMessageForHelp() {
        String response = askChoiceState.handleMessage(HELP_MSG);
        Assertions.assertEquals(CHOICE_HELP_REPLY, response);
    }

    @Test
    void testHandleOjekMessage() {
        when(transportStateGenerator.generateOjekStates(transportUser)).thenReturn(arbitraryState);
        when(arbitraryState.getOpeningMessage()).thenReturn(SAMPLE_REPLY);

        String response = askChoiceState.handleMessage(OJEK_MSG);

        Assertions.assertEquals(TransportMode.OJEK, transportUser.getTransportMode());
        Assertions.assertEquals(transportUser.getCurrentState(), arbitraryState);
        Assertions.assertNotNull(response);
    }

    @Test
    void testHandleTaxiMessage() {
        when(transportStateGenerator.generateTaxiStates(transportUser)).thenReturn(arbitraryState);
        when(arbitraryState.getOpeningMessage()).thenReturn(SAMPLE_REPLY);

        String response = askChoiceState.handleMessage(TAXI_MSG);

        // Should still be null as decision is not made here
        Assertions.assertNull(transportUser.getTransportMode());
        Assertions.assertNotNull(response);
    }

    @Test
    void testHandleHeliMessage() {
        when(transportStateGenerator.generateHeliStates(transportUser)).thenReturn(arbitraryState);
        when(arbitraryState.getOpeningMessage()).thenReturn(SAMPLE_REPLY);

        String response = askChoiceState.handleMessage(HELI_MSG);

        Assertions.assertEquals(TransportMode.HELICOPTER, transportUser.getTransportMode());
        Assertions.assertNotNull(response);
    }

    @Test
    void testHandleInvalidMessage() {
        String response = askChoiceState.handleMessage(INVALID_MSG);

        Assertions.assertEquals(NOT_FOUND_REPLY, response);
        Assertions.assertNull(transportUser.getTransportMode());
    }

    @Test
    void testOpeningMessageShouldBeCorrect() {
        String response = askChoiceState.getOpeningMessage();
        Assertions.assertEquals(TRANSPORT_CHOICE_REPLY, response);
    }

}
