package csui.serbagunabot.transportasi.repository;

import csui.serbagunabot.transportasi.model.TransportUser;

public interface TransportUserRepository {

    public TransportUser findByUserId(String userId);

    public TransportUser createUserFromId(String userId);

}
