package csui.serbagunabot.transportasi.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import csui.serbagunabot.transportasi.model.Driver;
import csui.serbagunabot.transportasi.repository.DriverStateRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DriverHandlerTest {

    @Mock
    Driver driver;

    @Mock
    DriverStateRepository driverStateRepository;

    @InjectMocks
    DriverHandlerService driverHandlerService;

    private static final String SAMPLE_MESSAGE = "Hello";
    private static final String SAMPLE_ID = "ABCXYZ";

    @Test
    void testHandleMessageUserFound() {
        when(driverStateRepository.findByUserId(SAMPLE_ID)).thenReturn(driver);
        driverHandlerService.handleMessage(SAMPLE_MESSAGE, SAMPLE_ID);

        verify(driverStateRepository, times(1))
            .findByUserId(SAMPLE_ID);
        verify(driverStateRepository, times(0))
            .createUserFromId(SAMPLE_ID);
        verify(driver, times(1)).respond(anyString());
    }

    @Test
    void testHandleMessageUserNotFoundShouldCreateUser() {
        lenient().when(driverStateRepository.findByUserId(SAMPLE_ID)).thenReturn(null);
        when(driverStateRepository.createUserFromId(SAMPLE_ID)).thenReturn(driver);
        driverHandlerService.handleMessage(SAMPLE_MESSAGE, SAMPLE_ID);

        verify(driverStateRepository, times(1))
            .findByUserId(SAMPLE_ID);
        verify(driverStateRepository, times(1))
            .createUserFromId(SAMPLE_ID);
        verify(driver, times(1)).respond(anyString());

    }

}
