package csui.serbagunabot.transportasi.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TransportModeTest {

    private static final long OJEK_COST = 2500;
    private static final long TAXI_LARGE_COST = 10000;
    private static final long TAXI_STD_COST = 8000;
    private static final long HELI_COST = 750000;

    private static final String OJEK_ALIAS = "Ojek";
    private static final String TAXI_LARGE_ALIAS = "Taksi Large";
    private static final String TAXI_STD_ALIAS = "Taksi Standard";
    private static final String HELI_ALIAS = "Helikopter";

    @Test
    void testOjekCost() {
        Assertions.assertEquals(OJEK_COST, TransportMode.OJEK.getCostPerKm());
    }

    @Test
    void testTaxiLargeCost() {
        Assertions.assertEquals(TAXI_LARGE_COST, TransportMode.TAXI_LARGE.getCostPerKm());
    }

    @Test
    void testTaxiStandardCost() {
        Assertions.assertEquals(TAXI_STD_COST, TransportMode.TAXI_STANDARD.getCostPerKm());
    }

    @Test
    void testHelicopterCost() {
        Assertions.assertEquals(HELI_COST, TransportMode.HELICOPTER.getCostPerKm());
    }

    @Test
    void testOjekAlias() {
        Assertions.assertEquals(OJEK_ALIAS, TransportMode.OJEK.getAlias());
    }

    @Test
    void testTaxiLargeAlias() {
        Assertions.assertEquals(TAXI_LARGE_ALIAS, TransportMode.TAXI_LARGE.getAlias());
    }

    @Test
    void testTaxiStandardAlias() {
        Assertions.assertEquals(TAXI_STD_ALIAS, TransportMode.TAXI_STANDARD.getAlias());
    }

    @Test
    void testHelicopterAlias() {
        Assertions.assertEquals(HELI_ALIAS, TransportMode.HELICOPTER.getAlias());
    }

}
