package csui.serbagunabot.transportasi.map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.google.gson.Gson;
import csui.serbagunabot.transportasi.map.exception.AddressNotFoundException;
import csui.serbagunabot.transportasi.map.model.BingMapResponse;
import csui.serbagunabot.transportasi.map.model.BingResource;
import csui.serbagunabot.transportasi.map.model.BingResourceSet;
import java.util.LinkedList;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BingMapUtilTest {

    @Mock
    OkHttpClient okHttpClient;

    @Mock
    Gson gson;

    @Mock
    Call call;

    @Mock
    Response response;

    @Mock
    ResponseBody responseBody;

    @InjectMocks
    BingMapUtil bingMapUtil;

    BingMapResponse successBingMapResponse;
    BingMapResponse failBingMapResponse404;
    BingMapResponse failBingMapResponse400;

    private static final String ADDRESS_SAMPLE_1 = "North Jakarta";
    private static final String ADDRESS_SAMPLE_2 = "Central Jakarta";
    private static final double SAMPLE_DISTANCE = 7.5;

    @BeforeEach
    void setUp() {
        BingResource bingResource = new BingResource(SAMPLE_DISTANCE);

        LinkedList<BingResource> successResourceList = new LinkedList<>();
        successResourceList.add(bingResource);

        LinkedList<BingResourceSet> resourceSetList = new LinkedList<>();
        BingResourceSet bingResourceSet = new BingResourceSet(successResourceList);
        resourceSetList.add(bingResourceSet);


        successBingMapResponse = new BingMapResponse(200,
            resourceSetList);

        failBingMapResponse404 = new BingMapResponse(404,
            new LinkedList<>());

        failBingMapResponse400 = new BingMapResponse(400,
            new LinkedList<>());

    }

    @Test
    void testGetDrivingDistanceFound() throws Exception {
        when(okHttpClient.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.body()).thenReturn(responseBody);
        when(responseBody.string()).thenReturn("{\"hello\":\"world\"}");

        when(gson.fromJson(anyString(), any()))
            .thenReturn(successBingMapResponse);

        double response = bingMapUtil.getDrivingDistance(ADDRESS_SAMPLE_1, ADDRESS_SAMPLE_2);
        Assertions.assertEquals(SAMPLE_DISTANCE, response);
    }

    @Test
    void testGetDrivingDistanceNotFound404() throws Exception {
        when(okHttpClient.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.body()).thenReturn(responseBody);
        when(responseBody.string()).thenReturn("{\"hello\":\"world\"}");

        when(gson.fromJson(anyString(), any()))
            .thenReturn(failBingMapResponse404);

        Assertions.assertThrows(AddressNotFoundException.class, () -> {
            bingMapUtil.getDrivingDistance(ADDRESS_SAMPLE_1, ADDRESS_SAMPLE_2);
        });
    }

    @Test
    void testGetDrivingDistanceNotFound400() throws Exception {
        when(okHttpClient.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.body()).thenReturn(responseBody);
        when(responseBody.string()).thenReturn("{\"hello\":\"world\"}");

        when(gson.fromJson(anyString(), any()))
            .thenReturn(failBingMapResponse400);

        Assertions.assertThrows(AddressNotFoundException.class, () -> {
            bingMapUtil.getDrivingDistance(ADDRESS_SAMPLE_1, ADDRESS_SAMPLE_2);
        });
    }

}
