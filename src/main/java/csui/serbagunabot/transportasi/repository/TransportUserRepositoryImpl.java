package csui.serbagunabot.transportasi.repository;

import csui.serbagunabot.transportasi.chat.generator.TransportStateGenerator;
import csui.serbagunabot.transportasi.model.TransportUser;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TransportUserRepositoryImpl implements TransportUserRepository {

    @Autowired
    TransportStateGenerator transportStateGenerator;

    Map<String, TransportUser> idToUserMap = new HashMap<>();

    @Override
    public TransportUser findByUserId(String userId) {
        return idToUserMap.get(userId);
    }

    @Override
    public TransportUser createUserFromId(String userId) {
        TransportUser newUser = new TransportUser();
        newUser.setCurrentState(transportStateGenerator
            .generateStartingStates(newUser));

        idToUserMap.put(userId, newUser);
        return findByUserId(userId);
    }
}
