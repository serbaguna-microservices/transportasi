package csui.serbagunabot.transportasi.repository;

import csui.serbagunabot.transportasi.model.Driver;

public interface DriverStateRepository {

    public Driver findByUserId(String userId);

    public Driver createUserFromId(String userId);

}
