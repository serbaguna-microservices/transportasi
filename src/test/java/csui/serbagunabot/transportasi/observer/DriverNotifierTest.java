package csui.serbagunabot.transportasi.observer;

import static org.mockito.Mockito.*;

import csui.serbagunabot.transportasi.feign.LinePushFeign;
import csui.serbagunabot.transportasi.map.MapUtil;
import csui.serbagunabot.transportasi.model.Driver;
import csui.serbagunabot.transportasi.model.TransportMode;
import csui.serbagunabot.transportasi.model.TransportUser;
import csui.serbagunabot.transportasi.repository.DriverRepository;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DriverNotifierTest {

    @Mock
    DriverRepository driverRepository;

    @Mock
    LinePushFeign linePushFeign;

    @Mock
    MapUtil mapUtil;

    @InjectMocks
    DriverNotifier driverNotifier;

    private TransportUser orderingUser;
    private List<Driver> listDriver;
    private Driver driver;
    private static final String LINE_ID_SAMPLE = "ABCDEFG";
    private static final String PICKUP_SAMPLE = "North Jakarta";
    private static final String DESTINATION_SAMPLE = "Central Jakarta";
    private static final String VEHICLE_TYPE = TransportMode.HELICOPTER.getAlias();

    @BeforeEach
    void setUp() {
        orderingUser = new TransportUser();
        orderingUser.setPickupAddress(PICKUP_SAMPLE);
        orderingUser.setDestinationAddress(DESTINATION_SAMPLE);
        orderingUser.setTransportMode(TransportMode.HELICOPTER);

        driver = new Driver();
        driver.setLineUserId(LINE_ID_SAMPLE);
        driver.setVehicleType(VEHICLE_TYPE);

        listDriver = new LinkedList<>();
        listDriver.add(driver);
    }

    @Test
    void testNotifyDriverForOneUserWithAvailableDriver() {
        when(driverRepository.findAllByVehicleType(VEHICLE_TYPE)).thenReturn(listDriver);
        Driver foundDriver = driverNotifier.notifyOneDriverForUser(orderingUser);
        Assertions.assertEquals(driver, foundDriver);
        verify(linePushFeign, times(1)).sendPushMessage(any());
    }

    @Test
    void testNotifyDriverUnavailableType() {
        when(driverRepository.findAllByVehicleType(VEHICLE_TYPE)).thenReturn(new LinkedList<>());
        Driver foundDriver = driverNotifier.notifyOneDriverForUser(orderingUser);

        Assertions.assertNull(foundDriver);
        verify(linePushFeign, times(0)).sendPushMessage(any());
    }

    @Test
    void testNotifyDriverMapError() throws Exception {
        when(mapUtil
            .getDrivingDistance(orderingUser.getPickupAddress(), orderingUser
                .getDestinationAddress())).thenThrow(new IOException());


        when(driverRepository.findAllByVehicleType(VEHICLE_TYPE)).thenReturn(listDriver);
        Driver foundDriver = driverNotifier.notifyOneDriverForUser(orderingUser);
        Assertions.assertEquals(driver, foundDriver);
        verify(linePushFeign, times(1)).sendPushMessage(any());
    }



}
