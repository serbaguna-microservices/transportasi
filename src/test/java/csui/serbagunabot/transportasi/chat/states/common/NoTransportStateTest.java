package csui.serbagunabot.transportasi.chat.states.common;

import csui.serbagunabot.transportasi.model.TransportUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class NoTransportStateTest {

    private NoTransportState noTransportState;
    private TransportUser transportUser;
    private static final String INVALID_MSG = "Hello World!";
    private static final String VALID_MSG = "";
    private static final String OPENING_REPLY = "Selamat datang di "
            + "fitur pemesanan transportasi";

    private static final String NOT_FOUND_REPLY = "Perintah tidak dikenal!\n"
            + "Ketik !transport untuk mulai menggunakan fitur transportasi";

    /**
     * Sets up the TransportUser holding the state and also the dummy next state.
     */
    @BeforeEach
    public void setUp() {
        transportUser = new TransportUser();
        noTransportState = new NoTransportState(transportUser);
        noTransportState.setNextState(new NoTransportState(transportUser));
    }

    @Test
    void testNoStateInvalidMessage() {
        String response = noTransportState.handleMessage(INVALID_MSG);
        Assertions.assertEquals(NOT_FOUND_REPLY, response);
    }

    @Test
    void testCorrectMessageShouldMoveToNextState() {
        noTransportState.handleMessage(VALID_MSG);
        Assertions.assertNotEquals(noTransportState, transportUser
                .getCurrentState());
    }

    @Test
    void testGetOpeningMessage() {
        String response = noTransportState.getOpeningMessage();
        Assertions.assertEquals(OPENING_REPLY, response);
    }


}
