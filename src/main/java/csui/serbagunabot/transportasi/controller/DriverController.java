package csui.serbagunabot.transportasi.controller;

import csui.serbagunabot.transportasi.model.line.LineRequest;
import csui.serbagunabot.transportasi.model.line.LineResponse;
import csui.serbagunabot.transportasi.service.DriverHandlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/driver")
public class DriverController {

    @Autowired
    DriverHandlerService driverHandlerService;

    /**
     * Receives !driver commands from proxy.
     */
    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<LineResponse> replyEntrypoint(@RequestBody LineRequest lineRequest) {
        String response = driverHandlerService
            .handleMessage(lineRequest.getMessage(), lineRequest.getUserId());

        return ResponseEntity.ok(new LineResponse(response));
    }

}
