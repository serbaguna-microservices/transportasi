package csui.serbagunabot.transportasi.spring;

import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OkHttpClientCustomFactory {
    @Bean
    public OkHttpClient okHttpClientCustom() {
        return new OkHttpClient();
    }
}
