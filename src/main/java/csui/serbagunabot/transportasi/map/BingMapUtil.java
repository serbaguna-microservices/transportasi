package csui.serbagunabot.transportasi.map;

import com.google.gson.Gson;
import csui.serbagunabot.transportasi.map.exception.AddressNotFoundException;
import csui.serbagunabot.transportasi.map.model.BingMapResponse;
import java.io.IOException;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BingMapUtil implements MapUtil {

    @Autowired
    OkHttpClient okHttpClientCustom;

    @Autowired
    Gson gson;

    @Value("${bing_api_key}")
    private String bingApiKey;

    private static final String DISTANCE_UNIT = "km";


    @Override
    public double getDrivingDistance(String pickupLocation, String destinationLocation)
        throws IOException, AddressNotFoundException {

        Response response = okHttpClientCustom
            .newCall(new Request.Builder()
                .url(buildBingMapsUrl(pickupLocation, destinationLocation))
                .build())
            .execute();

        BingMapResponse bingMapResponse = gson.fromJson(response.body().string(),
            BingMapResponse.class);

        if (bingMapResponse.getStatusCode() == 404 || bingMapResponse.getStatusCode() == 400) {
            throw new AddressNotFoundException();
        }

        return bingMapResponse
            .getResourceSets()
            .get(0).getResources().get(0).getTravelDistance();

    }

    private HttpUrl buildBingMapsUrl(String pickup, String destination) {
        return new HttpUrl.Builder()
            .host("dev.virtualearth.net")
            .scheme("http")
            .addPathSegment("REST/v1/Routes")
            .addQueryParameter("wayPoint.1", pickup)
            .addQueryParameter("wayPoint.2", destination)
            .addQueryParameter("distanceUnit", DISTANCE_UNIT)
            .addQueryParameter("key", bingApiKey)
            .build();
    }
}
