package csui.serbagunabot.transportasi.repository;

import csui.serbagunabot.transportasi.chat.generator.DriverStateGenerator;
import csui.serbagunabot.transportasi.model.Driver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DriverStateRepositoryTest {

    private Driver driver;
    private static final String SAMPLE_ID = "ABCXYZ";

    @Mock
    DriverStateGenerator driverStateGenerator;

    @InjectMocks
    DriverStateRepositoryImpl driverStateRepository;

    @BeforeEach
    void setUp() {
        driver = new Driver();
    }

    @Test
    void testCreateUserFromIdAndFindShouldFindSameDriver() {
        Driver result = driverStateRepository.createUserFromId(SAMPLE_ID);
        Assertions.assertEquals(result, driverStateRepository.findByUserId(SAMPLE_ID));
    }
}
