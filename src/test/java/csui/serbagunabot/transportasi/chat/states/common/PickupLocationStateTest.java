package csui.serbagunabot.transportasi.chat.states.common;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PickupLocationStateTest {

    private PickupLocationState pickupLocationState;
    private TransportUser transportUser;
    private TransportState mockNextState;

    private static final String SAMPLE_REPLY = "Hello World";
    private static final String SAMPLE_ADDRESS = "North Jakarta";
    private static final String OPENING_MSG = "Silahkan masukkan alamat tempat "
        + "anda ingin di pickup sekarang.\n\n"
        + "Contoh jawaban: \n"
        + "'!transport Stasiun Universitas Indonesia, Pondok Cina, Depok' \n";

    /**
     * Sets up TransportUser in PickUpLocation state and the mock next states.
     */
    @BeforeEach
    public void setUp() {
        transportUser = new TransportUser();
        pickupLocationState = new PickupLocationState(transportUser);
        mockNextState = mock(TransportState.class);
        pickupLocationState.setNextState(mockNextState);
    }

    @Test
    void testReceivePickupAddress() {
        when(mockNextState.getOpeningMessage()).thenReturn(SAMPLE_REPLY);
        String response = pickupLocationState.handleMessage(SAMPLE_ADDRESS);

        Assertions.assertEquals(transportUser.getCurrentState(), mockNextState);
        Assertions.assertEquals(SAMPLE_REPLY, response);
    }

    @Test
    void testOpeningMessage() {
        Assertions.assertEquals(OPENING_MSG, pickupLocationState.getOpeningMessage());
    }

}
