package csui.serbagunabot.transportasi.chat.states.taxi;

import csui.serbagunabot.transportasi.chat.generator.TransportStateGenerator;
import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.map.MapUtil;
import csui.serbagunabot.transportasi.map.exception.AddressNotFoundException;
import csui.serbagunabot.transportasi.model.Driver;
import csui.serbagunabot.transportasi.model.TransportMode;
import csui.serbagunabot.transportasi.model.TransportUser;
import csui.serbagunabot.transportasi.observer.DriverNotifier;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

public class ConfirmTaxiOrderState extends TransportState {

    @Autowired
    DriverNotifier driverNotifier;

    @Autowired
    MapUtil mapUtil;

    @Autowired
    TransportStateGenerator transportStateGenerator;

    @Setter
    private TransportState reinputState;

    @Setter
    private String closingMessage = "Terima kasih atas pesanan anda.";

    private static final String INVALID_MSG = "Perintah anda tidak dikenal!\n"
        + "Ketik: \n"
        + "-) '!transport iya' untuk konfirmasi pesanan anda\n"
        + "-) '!transport ulang' untuk input ulang alamat pickup dan tujuan anda.\n"
        + "-) '!transport ganti' untuk mengganti ukuran taksi anda.";

    private static final String NO_DRIVER_MSG = "Mohon maaf, saat ini kami tidak menemukan "
        + "pengemudi untuk tipe kendaraan yang anda telah pilih.\n Mohon memesan kembali di lain "
        + "waktu dengan perintah '!transport'";

    public ConfirmTaxiOrderState(TransportUser transportUser) {
        super(transportUser);
    }

    @Override
    public String handleMessage(String message) {
        switch (message) {
            case "iya":
                // Regenerates base transport states
                Driver driver = driverNotifier.notifyOneDriverForUser(transportUser);
                transportUser
                    .setCurrentState(transportStateGenerator
                        .generateStartingStates(transportUser));
                if (driver == null) {
                    return NO_DRIVER_MSG;
                }
                return closingMessage + "\n\n" + getDriverCredentials(driver);

            case "ulang":
                // Moves back to reinput state
                transportUser
                    .setCurrentState(reinputState);
                return reinputState.getOpeningMessage();

            case "ganti":
                // Change to large or standard based on current taxi mode
                if (transportUser.getTransportMode() == TransportMode.TAXI_LARGE) {
                    transportUser.setTransportMode(TransportMode.TAXI_STANDARD);
                } else {
                    transportUser.setTransportMode(TransportMode.TAXI_LARGE);
                }
                return getOpeningMessage();

            default:
                return INVALID_MSG;
        }
    }

    @Override
    public String getOpeningMessage() {
        try {
            double distance = mapUtil.getDrivingDistance(transportUser
                .getPickupAddress(), transportUser.getDestinationAddress());

            long cost = Math.round(distance) * transportUser.getTransportMode().getCostPerKm();

            return "Anda akan memesan layanan "
                + transportUser.getTransportMode().getAlias()
                + " untuk tujuan dari:\n\n"
                + transportUser.getPickupAddress() + "\n\n"
                + "menuju\n\n"
                + transportUser.getDestinationAddress() + "\n\n"
                + "Untuk biaya sebesar Rp "
                + NumberFormat.getNumberInstance(Locale.US).format(cost) + ".00\n"
                + "dengan jarak berkendara " + distance + " km\n\n"
                + "Apakah anda konfirmasi pemesanan ini?\n"
                + "Ketik:\n"
                + "-) '!transport iya' untuk konfirmasi\n"
                + "-) '!transport ulang' untuk mengubah alamat pickup dan tujuan.\n"
                + "-) '!transport ganti' untuk mengganti ukuran taksi anda.";
        } catch (IOException e) {
            transportUser.setCurrentState(reinputState);
            return "Sedang ada masalah dalam perhitungan"
                + " jarak, mohon menginput ulang"
                + " mulai dari alamat pickup anda.";

        } catch (AddressNotFoundException e) {
            transportUser.setCurrentState(reinputState);
            return "Alamat / rute tidak ditemukan!\n"
                + "Mohon memasukkan ulang alamat mulai dari alamat pickup anda.";
        }

    }

    private String getDriverCredentials(Driver driver) {
        return "Nama driver: " + driver.getName() + "\n"
            + "Nomor polisi kendaraan: " + driver.getPlateNumber();
    }
}
