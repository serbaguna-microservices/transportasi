package csui.serbagunabot.transportasi.chat.states.helikopter;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportUser;
import lombok.Setter;

public class HelicopterConfirmState extends TransportState {

    @Setter
    private TransportState acceptState;

    @Setter
    private TransportState rejectState;

    private static final String REJECT_MESSAGE = "Pemesanan helikopter telah dibatalkan.\n"
        + "Silahkan melakukan pemesanan transportasi dengan perintah '!transport' kembali.";

    private static final String HELP_MESSAGE = "Perintah tidak dikenal!\n"
        + "Ketik '!transport iya' untuk konfirmasi ketersediaan layanan atau\n"
        + "Ketik '!transport tidak' untuk batal pemesanan helikopter.";

    public HelicopterConfirmState(TransportUser transportUser) {
        super(transportUser);
    }

    @Override
    public String handleMessage(String message) {
        switch (message) {
            case "iya":
                transportUser.setCurrentState(acceptState);
                return acceptState.getOpeningMessage();
            case "tidak":
                transportUser.setCurrentState(rejectState);
                return REJECT_MESSAGE;
            default:
                return HELP_MESSAGE;
        }
    }

    @Override
    public String getOpeningMessage() {
        return "Perhatian! Pemesanan helikopter baru hanya tersedia di daerah berikut:\n"
            + "1) DKI Jakarta dan Tanggerang\n"
            + "2) Depok\n"
            + "Serta lokasi pickup anda harus berada di Helipad atau ruang terbuka yang luas\n\n"
            + "Tujuan dari pemesanan helikopter ini juga hanya terbatas ke bandara tertentu"
            + "di daerah Jakarta, Tanggeran, dan Depok\n\n"
            + "Apakah anda konfirmasi dengan ketentuan berikut?\n"
            + "Ketik:\n"
            + "'!transport iya' untuk konfirmasi\n"
            + "'!transport tidak' untuk batal pemesanan helikopter";
    }
}
