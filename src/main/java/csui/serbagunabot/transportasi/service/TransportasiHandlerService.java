package csui.serbagunabot.transportasi.service;

import csui.serbagunabot.transportasi.model.TransportUser;
import csui.serbagunabot.transportasi.repository.TransportUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransportasiHandlerService {

    @Autowired
    TransportUserRepository transportUserRepository;

    Logger logger = LoggerFactory.getLogger("Reply Logging");

    /**
     * Handles message for transportasi logic.
     * @param message message from the proxy server
     * @param userId userId as the sending user's id
     * @return String as a reply
     */
    public String handleMessage(String message, String userId) {
        String cleanMessage = message.replace("!transport", "").trim();
        logger.info("Received transport message {} from {}", message, userId);

        TransportUser transportUser = transportUserRepository
                .findByUserId(userId);

        if (transportUser == null) {
            transportUser = transportUserRepository.createUserFromId(userId);
        }

        return transportUser.respond(cleanMessage);
    }
}
