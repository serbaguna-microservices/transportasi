package csui.serbagunabot.transportasi.chat.states.driver;

import csui.serbagunabot.transportasi.chat.states.DriverState;
import csui.serbagunabot.transportasi.model.Driver;
import lombok.Setter;

public class DriverStartState extends DriverState {

    @Setter
    private DriverState nextState;

    private static final String NOT_FOUND_MSG = "Perintah tidak dikenal!\n"
        + "Ketik !driver untuk mulai menggunakan fitur registrasi driver";

    public DriverStartState(Driver driver) {
        super(driver);
    }

    @Override
    public String handleMessage(String message) {
        if (message.isEmpty()) {
            this.driver.setCurrentState(nextState);
            return nextState.getOpeningMessage();
        }
        return NOT_FOUND_MSG;
    }

    @Override
    public String getOpeningMessage() {
        return "Selamat datang di fitur driver registration!";
    }
}
