package csui.serbagunabot.transportasi.chat.states.common;

import static org.mockito.Mockito.when;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DestinationLocationStateTest {

    private static final String SAMPLE_ADDRESS = "North Jakarta";

    private static final String SAMPLE_REPLY = "ABC";

    private static final String OPENING_MSG = "Silahkan masukkan alamat tujuan "
        + "bepergian anda dengan contoh:\n"
        + "'!transport Stasiun Universitas Indonesia, Pondok Cina, Depok'";

    @Mock
    TransportState arbitraryState;

    TransportUser transportUser;

    private DestinationLocationState destinationLocationState;

    @BeforeEach
    void setUp() {
        transportUser = new TransportUser();
        transportUser.setCurrentState(destinationLocationState);
        destinationLocationState = new DestinationLocationState(transportUser);
        destinationLocationState.setNextState(arbitraryState);
    }

    @Test
    void testHandleMessage() {
        when(arbitraryState.getOpeningMessage()).thenReturn(SAMPLE_REPLY);
        String response = destinationLocationState.handleMessage(SAMPLE_ADDRESS);

        Assertions.assertEquals(SAMPLE_ADDRESS, transportUser.getDestinationAddress());
        Assertions.assertEquals(arbitraryState, transportUser.getCurrentState());
        Assertions.assertEquals(SAMPLE_REPLY, response);
    }

    @Test
    void testOpeningMessage() {
        Assertions.assertEquals(OPENING_MSG, destinationLocationState.getOpeningMessage());
    }

}
