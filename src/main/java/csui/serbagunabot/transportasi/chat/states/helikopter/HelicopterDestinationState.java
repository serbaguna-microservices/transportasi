package csui.serbagunabot.transportasi.chat.states.helikopter;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportUser;
import lombok.Setter;

public class HelicopterDestinationState extends TransportState {

    private static final String ASK_LIST_MSG = "List dari tujuan tersedia sebagai berikut:\n";

    private static final String AIRPORT_LIST = "1) Soekarno-Hatta International Airport\n"
        + "2) Halim Perdanakusuma International Airport\n"
        + "3) Kemayoran International Airport\n"
        + "4) Bandar Udara Pondok Cabe\n"
        + "Ketik '!transport n' untuk memilih, dengan n adalah nomor pilihan anda.";

    private static final String INVALID_MSG = "Pilihan tidak dikenal!\n"
        + "Ketik '!transport list' untuk menampilkan list tujuan yang tersedia.";

    @Setter
    private TransportState nextState;

    public HelicopterDestinationState(TransportUser transportUser) {
        super(transportUser);
    }

    @Override
    public String handleMessage(String message) {
        switch (message) {
            case "1":
                transportUser.setDestinationAddress("Soekarno-Hatta International Airport");
                break;
            case "2":
                transportUser.setDestinationAddress("Halim Perdanakusuma International Airport");
                break;
            case "3":
                transportUser.setDestinationAddress("Kemayoran International Airport");
                break;
            case "4":
                transportUser.setDestinationAddress("Bandar Udara Pondok Cabe");
                break;
            case "list":
                return ASK_LIST_MSG + AIRPORT_LIST;
            default:
                return INVALID_MSG;
        }

        transportUser.setCurrentState(nextState);
        return nextState.getOpeningMessage();
    }

    @Override
    public String getOpeningMessage() {
        return "Silahkan input bandara tujuan anda, berikut tujuan yang tersedia:\n"
            + AIRPORT_LIST;
    }
}
