package csui.serbagunabot.transportasi.chat.states.taxi;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportMode;
import csui.serbagunabot.transportasi.model.TransportUser;
import lombok.Setter;

public class AskTaxiModeState extends TransportState {

    @Setter
    private TransportState nextState;

    public static final String INVALID_CHOICE_REPLY = "Pilihan tidak dikenal!\n"
        + "Ketik '!transport standard' atau '!transport large' untuk "
        + "memilih ukuran taksi yang anda akan pesan.";

    public AskTaxiModeState(TransportUser transportUser) {
        super(transportUser);
    }

    @Override
    public String handleMessage(String message) {
        switch (message) {
            case "large":
                transportUser.setTransportMode(TransportMode.TAXI_LARGE);
                transportUser.setCurrentState(nextState);
                return nextState.getOpeningMessage();
            case "standard":
                transportUser.setTransportMode(TransportMode.TAXI_STANDARD);
                transportUser.setCurrentState(nextState);
                return nextState.getOpeningMessage();
            default:
                return INVALID_CHOICE_REPLY;
        }
    }

    @Override
    public String getOpeningMessage() {
        return "Silahkan memilih ukuran mobil taksi dengan mengetik:\n"
            + "-) '!transport standard' untuk ukuran standar (2-3 orang)\n"
            + "-) '!transport large' untuk ukuran large (4-5 orang)";
    }
}
