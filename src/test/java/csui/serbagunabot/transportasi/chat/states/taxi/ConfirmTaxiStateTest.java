package csui.serbagunabot.transportasi.chat.states.taxi;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import csui.serbagunabot.transportasi.chat.generator.TransportStateGenerator;
import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.map.MapUtil;
import csui.serbagunabot.transportasi.map.exception.AddressNotFoundException;
import csui.serbagunabot.transportasi.model.Driver;
import csui.serbagunabot.transportasi.model.TransportMode;
import csui.serbagunabot.transportasi.model.TransportUser;
import csui.serbagunabot.transportasi.observer.DriverNotifier;
import java.io.IOException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ConfirmTaxiStateTest {

    private static final String CONFIRM_MSG = "iya";
    private static final String REINPUT_MSG = "ulang";
    private static final String CHANGE_MSG = "ganti";
    private static final String INVALID_MSG = "xyz";

    private static final String CLOSING_REPLY = "Terima kasih atas pesanan anda.";
    private static final String SAMPLE_REPLY = "abc";

    private static final String ADDRESS_PICKUP = "North Jakarta";
    private static final String ADDRESS_DESTINATION = "Central Jakarta";
    private static final double SAMPLE_DISTANCE = 6.7;
    private static final String OPENING_REPLY = "Anda akan memesan layanan "
        + "Taksi Large"
        + " untuk tujuan dari:\n\n"
        + ADDRESS_PICKUP + "\n\n"
        + "menuju\n\n"
        + ADDRESS_DESTINATION + "\n\n"
        + "Untuk biaya sebesar Rp "
        + "70,000" + ".00\n"
        + "dengan jarak berkendara 6.7 km\n\n"
        + "Apakah anda konfirmasi pemesanan ini?\n"
        + "Ketik:\n"
        + "-) '!transport iya' untuk konfirmasi\n"
        + "-) '!transport ulang' untuk mengubah alamat pickup dan tujuan.\n"
        + "-) '!transport ganti' untuk mengganti ukuran taksi anda.";

    private static final String INVALID_REPLY = "Perintah anda tidak dikenal!\n"
        + "Ketik: \n"
        + "-) '!transport iya' untuk konfirmasi pesanan anda\n"
        + "-) '!transport ulang' untuk input ulang alamat pickup dan tujuan anda.\n"
        + "-) '!transport ganti' untuk mengganti ukuran taksi anda.";

    private static final String ERROR_REPLY = "Sedang ada masalah dalam perhitungan"
        + " jarak, mohon menginput ulang"
        + " mulai dari alamat pickup anda.";

    private static final String INVALID_ADDRESS_REPLY = "Alamat / rute tidak ditemukan!\n"
        + "Mohon memasukkan ulang alamat mulai dari alamat pickup anda.";

    private static final String DRIVER_NAME = "Mock Driver";
    private static final String DRIVER_LICENSE = "B1234A";

    private static final String DRIVER_CREDS = "Nama driver: " + DRIVER_NAME + "\n"
        + "Nomor polisi kendaraan: " + DRIVER_LICENSE;

    private static final String NO_DRIVER_REPLY = "Mohon maaf, saat ini kami tidak menemukan "
        + "pengemudi untuk tipe kendaraan yang anda telah pilih.\n Mohon memesan kembali di lain "
        + "waktu dengan perintah '!transport'";

    @Mock
    Driver driver;

    @Mock
    DriverNotifier driverNotifier;

    @Mock
    TransportStateGenerator transportStateGenerator;

    @Mock
    MapUtil mapUtil;

    @Mock
    TransportState arbitraryState;

    private TransportUser transportUser = new TransportUser();

    @InjectMocks
    private ConfirmTaxiOrderState confirmTaxiState = new ConfirmTaxiOrderState(transportUser);

    @BeforeEach
    void setUp() {
        confirmTaxiState.setReinputState(arbitraryState);
        transportUser.setCurrentState(confirmTaxiState);

        driver = new Driver();
        driver.setName(DRIVER_NAME);
        driver.setPlateNumber(DRIVER_LICENSE);
    }

    @Test
    void testConfirmMessage() {
        when(transportStateGenerator.generateStartingStates(transportUser))
            .thenReturn(arbitraryState);
        when(driverNotifier.notifyOneDriverForUser(transportUser)).thenReturn(driver);
        String response = confirmTaxiState.handleMessage(CONFIRM_MSG);

        Assertions.assertEquals(CLOSING_REPLY + "\n\n" + DRIVER_CREDS, response);
        Assertions.assertEquals(transportUser.getCurrentState(), arbitraryState);
    }

    @Test
    void testConfirmMessageNoDriverAvailable() {
        when(transportStateGenerator.generateStartingStates(transportUser))
            .thenReturn(arbitraryState);
        lenient().when(driverNotifier.notifyOneDriverForUser(transportUser)).thenReturn(null);
        String response = confirmTaxiState.handleMessage(CONFIRM_MSG);
        Assertions.assertEquals(NO_DRIVER_REPLY, response);
        Assertions.assertEquals(transportUser.getCurrentState(), arbitraryState);
    }

    @Test
    void testReinputMessage() {
        when(arbitraryState.getOpeningMessage()).thenReturn(SAMPLE_REPLY);
        String response = confirmTaxiState.handleMessage(REINPUT_MSG);

        Assertions.assertEquals(SAMPLE_REPLY, response);
        Assertions.assertEquals(arbitraryState, transportUser.getCurrentState());
    }

    @Test
    void testChangeModeLargeToStandardMessage() {
        transportUser.setTransportMode(TransportMode.TAXI_LARGE);
        confirmTaxiState.handleMessage(CHANGE_MSG);

        Assertions.assertEquals(TransportMode.TAXI_STANDARD, transportUser.getTransportMode());
    }

    @Test
    void testChangeModeStandardToLargeMessage() {
        transportUser.setTransportMode(TransportMode.TAXI_STANDARD);
        confirmTaxiState.handleMessage(CHANGE_MSG);

        Assertions.assertEquals(TransportMode.TAXI_LARGE, transportUser.getTransportMode());
    }

    @Test
    void testInvalidMessage() {
        String response = confirmTaxiState.handleMessage(INVALID_MSG);
        Assertions.assertEquals(INVALID_REPLY, response);
    }

    @Test
    void testGetOpeningMessageLogic() throws Exception {
        when(mapUtil.getDrivingDistance(ADDRESS_PICKUP, ADDRESS_DESTINATION))
            .thenReturn(SAMPLE_DISTANCE);
        transportUser.setTransportMode(TransportMode.TAXI_LARGE);
        transportUser.setPickupAddress(ADDRESS_PICKUP);
        transportUser.setDestinationAddress(ADDRESS_DESTINATION);

        String message = confirmTaxiState.getOpeningMessage();
        Assertions.assertEquals(OPENING_REPLY, message);
    }

    @Test
    void testOpeningMessageErrorConnection() throws Exception {
        when(mapUtil.getDrivingDistance(transportUser
            .getPickupAddress(), transportUser.getDestinationAddress()))
            .thenThrow(new IOException());

        String response = confirmTaxiState.getOpeningMessage();

        Assertions.assertEquals(transportUser.getCurrentState(), arbitraryState);
        Assertions.assertEquals(ERROR_REPLY, response);
    }

    @Test
    void testInvalidAddressInput() throws Exception {
        when(mapUtil.getDrivingDistance(transportUser
            .getPickupAddress(), transportUser.getDestinationAddress()))
            .thenThrow(new AddressNotFoundException());

        String response = confirmTaxiState.getOpeningMessage();

        Assertions.assertEquals(transportUser.getCurrentState(), arbitraryState);
        Assertions.assertEquals(INVALID_ADDRESS_REPLY, response);
    }

}
