package csui.serbagunabot.transportasi.map.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class BingResource {

    private double travelDistance;

}
