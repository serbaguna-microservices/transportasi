package csui.serbagunabot.transportasi.model;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import csui.serbagunabot.transportasi.chat.states.DriverState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DriverTest {

    @Mock
    DriverState mockState;

    private Driver driver;
    private static final String SAMPLE_MSG = "ABCXYZ";

    @BeforeEach
    void setUp() {
        driver = new Driver();
        driver.setCurrentState(mockState);
    }

    @Test
    void testRespondState() {
        driver.respond(SAMPLE_MSG);
        verify(mockState, times(1)).handleMessage(SAMPLE_MSG);
    }

}
