package csui.serbagunabot.transportasi.repository;

import csui.serbagunabot.transportasi.chat.generator.DriverStateGenerator;
import csui.serbagunabot.transportasi.model.Driver;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DriverStateRepositoryImpl implements DriverStateRepository {

    @Autowired
    DriverStateGenerator driverStateGenerator;

    Map<String, Driver> idToDriverMap = new HashMap<>();

    @Override
    public Driver findByUserId(String userId) {
        return idToDriverMap.get(userId);
    }

    @Override
    public Driver createUserFromId(String userId) {
        Driver newDriver = new Driver();
        newDriver.setLineUserId(userId);
        newDriver.setCurrentState(driverStateGenerator.generateRegisterState(newDriver));
        idToDriverMap.put(userId, newDriver);

        return newDriver;
    }
}
