package csui.serbagunabot.transportasi.chat.states.driver;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import csui.serbagunabot.transportasi.chat.states.DriverState;
import csui.serbagunabot.transportasi.model.Driver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DriverStartStateTest {

    private static final String CORRECT_MSG = "";
    private static final String INVALID_MSG = "XYZ";

    private DriverStartState driverStartState;
    private Driver driver;

    @Mock
    DriverState mockState;

    private static final String NOT_FOUND_REPLY = "Perintah tidak dikenal!\n"
        + "Ketik !driver untuk mulai menggunakan fitur registrasi driver";

    private static final String OPENING_REPLY = "Selamat datang di fitur driver registration!";

    @BeforeEach
    void setUp() {
        driver = new Driver();
        driverStartState = new DriverStartState(driver);
        driver.setCurrentState(driverStartState);
        driverStartState.setNextState(mockState);
    }

    @Test
    void testHandleMessageEmptyShouldMoveOn() {
        driverStartState.handleMessage(CORRECT_MSG);
        verify(mockState, times(1)).getOpeningMessage();
        Assertions.assertEquals(mockState, driver.getCurrentState());
    }

    @Test
    void testHandleIncorrectMessageShouldWarn() {
        String response = driverStartState.handleMessage(INVALID_MSG);
        Assertions.assertEquals(NOT_FOUND_REPLY, response);
    }

    @Test
    void testOpeningMessage() {
        String response = driverStartState.getOpeningMessage();
        Assertions.assertEquals(OPENING_REPLY, response);
    }

}
