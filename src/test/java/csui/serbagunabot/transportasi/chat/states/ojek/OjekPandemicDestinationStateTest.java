package csui.serbagunabot.transportasi.chat.states.ojek;

import static org.mockito.Mockito.when;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.map.MapUtil;
import csui.serbagunabot.transportasi.map.exception.AddressNotFoundException;
import csui.serbagunabot.transportasi.model.TransportUser;
import java.io.IOException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OjekPandemicDestinationStateTest {

    @Mock
    private TransportState mockNextState;

    @Mock
    private TransportState mockInvalidDistanceState;

    @Mock
    private MapUtil mapUtil;

    private TransportUser transportUser = new TransportUser();

    @InjectMocks
    private OjekPandemicDestinationState ojekPandemicDestinationState =
        new OjekPandemicDestinationState(transportUser);

    private static final String SAMPLE_OP_MSG = "Hello World!";
    private static final String SAMPLE_ADDRESS = "North Jakarta";
    private static final double INVALID_DIST = 32.3;
    private static final double VALID_DIST = 5.4;

    private static final String INVALID_DIST_MSG = "Jarak berkendara pickup dan tujuan "
        + "lebih dari 8 km, silahkan memasukkan ulang "
        + "mulai dari alamat pickup anda.\n"
        + "Jarak berkendara pesanan anda sebelumnya: " + INVALID_DIST + " km";

    private static final String DEST_OPENING_MSG = "Silahkan masukkan alamat tempat tujuan anda\n"
        + "Peringatan: Layanan ojek hanya menerima jarak berkendara <= 8 km\n"
        + "Contoh jawaban:\n"
        + "'!transport Depok Town Square, Beji, Depok' \n";

    private static final String ERROR_REPLY = "Sedang ada masalah dalam perhitungan"
        + " jarak, mohon menginput ulang"
        + " mulai dari alamat pickup anda.";

    private static final String INVALID_ADDRESS_REPLY = "Alamat / rute tidak ditemukan!\n"
        + "Mohon memasukkan ulang alamat mulai dari alamat pickup anda.";

    @BeforeEach
    void setUp() {
        transportUser.setCurrentState(ojekPandemicDestinationState);
        ojekPandemicDestinationState.setNextState(mockNextState);
        ojekPandemicDestinationState.setLocationInvalidState(mockInvalidDistanceState);
    }

    @Test
    void testHandleMessageInvalidDistanceShouldReject() throws Exception {
        when(mapUtil.getDrivingDistance(transportUser.getPickupAddress(),
            SAMPLE_ADDRESS)).thenReturn(INVALID_DIST);

        String response = ojekPandemicDestinationState.handleMessage(SAMPLE_ADDRESS);
        Assertions.assertEquals(INVALID_DIST_MSG, response);
    }

    @Test
    void testHandleMessageValidDistanceShouldSucceed() throws Exception {
        when(mapUtil.getDrivingDistance(transportUser.getPickupAddress(),
            SAMPLE_ADDRESS)).thenReturn(VALID_DIST);

        when(mockNextState.getOpeningMessage()).thenReturn(SAMPLE_OP_MSG);

        String response = ojekPandemicDestinationState.handleMessage(SAMPLE_ADDRESS);
        Assertions.assertEquals(SAMPLE_OP_MSG, response);
        Assertions.assertEquals(transportUser.getCurrentState(), mockNextState);
    }

    @Test
    void testHandleMessageMapUtilError() throws Exception {
        when(mapUtil.getDrivingDistance(transportUser.getPickupAddress(),
            SAMPLE_ADDRESS)).thenThrow(new IOException());

        String response = ojekPandemicDestinationState.handleMessage(SAMPLE_ADDRESS);
        Assertions.assertEquals(mockInvalidDistanceState, transportUser.getCurrentState());
        Assertions.assertEquals(ERROR_REPLY, response);
    }

    @Test
    void testHandleMessageInvalidAddress() throws Exception {
        when(mapUtil.getDrivingDistance(transportUser.getPickupAddress(),
            SAMPLE_ADDRESS)).thenThrow(new AddressNotFoundException());

        String response = ojekPandemicDestinationState.handleMessage(SAMPLE_ADDRESS);
        Assertions.assertEquals(mockInvalidDistanceState, transportUser.getCurrentState());
        Assertions.assertEquals(INVALID_ADDRESS_REPLY, response);
    }

    @Test
    void testOpeningMessageShouldBeCorrect() {
        Assertions.assertEquals(DEST_OPENING_MSG, ojekPandemicDestinationState
            .getOpeningMessage());
    }


}
