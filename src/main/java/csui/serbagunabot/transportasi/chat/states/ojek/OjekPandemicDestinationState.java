package csui.serbagunabot.transportasi.chat.states.ojek;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.map.MapUtil;
import csui.serbagunabot.transportasi.map.exception.AddressNotFoundException;
import csui.serbagunabot.transportasi.model.TransportUser;
import java.io.IOException;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

public class OjekPandemicDestinationState extends TransportState {

    @Autowired
    MapUtil mapUtil;

    @Setter
    private TransportState nextState;

    @Setter
    private TransportState locationInvalidState;

    public OjekPandemicDestinationState(TransportUser transportUser) {
        super(transportUser);
    }

    @Override
    public String handleMessage(String message) {
        transportUser.setDestinationAddress(message);

        try {
            double distance = mapUtil.getDrivingDistance(transportUser
                .getPickupAddress(), transportUser
                .getDestinationAddress());

            // Go to state set as invalid distance handler
            if (distance > 8) {
                transportUser.setCurrentState(locationInvalidState);
                return getTooLargeDistanceMessage(distance);
            }

            // Go to next state set as "success" handler
            transportUser.setCurrentState(nextState);
            return nextState.getOpeningMessage();

        } catch (IOException e) {
            transportUser.setCurrentState(locationInvalidState);
            return "Sedang ada masalah dalam perhitungan"
                + " jarak, mohon menginput ulang"
                + " mulai dari alamat pickup anda.";

        } catch (AddressNotFoundException e) {
            transportUser.setCurrentState(locationInvalidState);
            return "Alamat / rute tidak ditemukan!\n"
                + "Mohon memasukkan ulang alamat mulai dari alamat pickup anda.";
        }

    }

    @Override
    public String getOpeningMessage() {
        return "Silahkan masukkan alamat tempat tujuan anda\n"
            + "Peringatan: Layanan ojek hanya menerima jarak berkendara <= 8 km\n"
            + "Contoh jawaban:\n"
            + "'!transport Depok Town Square, Beji, Depok' \n";
    }

    private String getTooLargeDistanceMessage(double actualDistance) {
        return "Jarak berkendara pickup dan tujuan "
            + "lebih dari 8 km, silahkan memasukkan ulang "
            + "mulai dari alamat pickup anda.\n"
            + "Jarak berkendara pesanan anda sebelumnya: " + actualDistance + " km";
    }
}
