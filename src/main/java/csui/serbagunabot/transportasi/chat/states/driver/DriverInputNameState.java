package csui.serbagunabot.transportasi.chat.states.driver;

import csui.serbagunabot.transportasi.chat.states.DriverState;
import csui.serbagunabot.transportasi.model.Driver;
import lombok.Setter;

public class DriverInputNameState extends DriverState {

    @Setter
    private DriverState nextState;

    public DriverInputNameState(Driver driver) {
        super(driver);
    }

    @Override
    public String handleMessage(String message) {
        this.driver.setName(message);
        this.driver.setCurrentState(nextState);
        return nextState.getOpeningMessage();
    }

    @Override
    public String getOpeningMessage() {
        return "Terima kasih untuk keinginan anda bergabung dengan kami.\n"
            + "Silahkan input nama lengkap anda untuk melanjutkan pendaftaran\n"
            + "Contoh: '!driver John Doe'";
    }
}
