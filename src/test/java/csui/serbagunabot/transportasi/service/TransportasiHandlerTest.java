package csui.serbagunabot.transportasi.service;

import static org.mockito.Mockito.*;

import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportUser;
import csui.serbagunabot.transportasi.repository.TransportUserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TransportasiHandlerTest {

    @InjectMocks
    private TransportasiHandlerService transportasiHandlerService;

    @Mock
    TransportUserRepository transportUserRepository;

    @Mock
    TransportState transportState;

    private TransportUser transportUser;

    private static final String USER_ID_SAMPLE = "ABCXYZ";
    private static final String COMMAND_SAMPLE = "Hello World!";
    private static final String REPLY_SAMPLE = "DEFGHI";

    @BeforeEach
    public void setUp() {
        transportUser = new TransportUser();
        transportUser.setCurrentState(transportState);
    }

    @Test
    void testMessageHandleWithExistingUserShouldReturnUser() {
        when(transportUserRepository.findByUserId(USER_ID_SAMPLE))
                .thenReturn(transportUser);

        when(transportUser.respond(COMMAND_SAMPLE)).thenReturn(REPLY_SAMPLE);

        String result = transportasiHandlerService
                .handleMessage(COMMAND_SAMPLE, USER_ID_SAMPLE);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(REPLY_SAMPLE, result);
        verify(transportUserRepository, times(0))
                .createUserFromId(USER_ID_SAMPLE);
    }

    @Test
    void testMessageHandleWithNonExistentUserShouldCreateUser() {
        when(transportUserRepository.createUserFromId(USER_ID_SAMPLE))
                .thenReturn(transportUser);

        when(transportUser.respond(COMMAND_SAMPLE)).thenReturn(REPLY_SAMPLE);

        String result = transportasiHandlerService
                .handleMessage(COMMAND_SAMPLE, USER_ID_SAMPLE);

        Assertions.assertNotNull(result);
        verify(transportUserRepository, times(1))
                .createUserFromId(USER_ID_SAMPLE);
    }
}
