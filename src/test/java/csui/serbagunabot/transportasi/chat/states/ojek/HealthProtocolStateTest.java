package csui.serbagunabot.transportasi.chat.states.ojek;

import static org.mockito.Mockito.when;

import csui.serbagunabot.transportasi.chat.generator.TransportStateGenerator;
import csui.serbagunabot.transportasi.chat.states.TransportState;
import csui.serbagunabot.transportasi.model.TransportUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HealthProtocolStateTest {

    private TransportUser transportUser = new TransportUser();

    @InjectMocks
    private HealthProtocolState healthProtocolState = new HealthProtocolState(transportUser);

    @Mock
    private TransportState mockAgreeState;

    @Mock
    private TransportState mockDeclineState;


    private static final String SAMPLE_RESPONSE = "Hello World!";
    private static final String DECLINE_REPLY = "Mohon maaf, untuk menggunakan layanan ojek, "
        + "anda wajib setuju dan mematuhi protokol kesehatan yang sudah tertera.\n\n"
        + "Silahkan melakukan pesanan kembali dengan perintah '!transport'";

    private static final String INVALID_REPLY = "Jawaban tidak dikenal, mohon menjawab dengan\n"
        + "'!transport iya' untuk setuju dengan protokol kesehatan\n"
        + "atau '!transport tidak' untuk tidak setuju dengan protokol kesehatan.";

    private static final String AGREE_MESSAGE = "iya";
    private static final String DISAGREE_MESSAGE = "tidak";
    private static final String INVALID_MESSAGE = "Hello World";

    private static final String OPENING_MESSAGE = "Perhatian! dikarenakan pandemi COVID-19, "
        + "Semua pelanggan transportasi ojek harus mengikuti protokol kesehatan sebagai berikut:\n"
        + "1) Helm harus disediakan oleh pelanggan masing-masing.\n"
        + "2) Wajib menggunakan masker selama perjalanan.\n"
        + "3) Tutup mulut anda dengan siku atau bahu saat bersin atau batuk.\n"
        + "4) Cuci tangan setidaknya selama 20 detik setelah berkendara.\n"
        + "5) Jarak berkendara antar lokasi wajib <= 8 km\n"
        + "Apabila anda setuju dengan protokol kesehatan maka\n\n"
        + "Ketik:\n"
        + "-) '!transport iya' untuk setuju\n"
        + "-) '!transport tidak' untuk tidak setuju";

    @BeforeEach
    void setUp() {
        transportUser.setCurrentState(healthProtocolState);
        healthProtocolState.setAgreeState(mockAgreeState);
        healthProtocolState.setDeclineState(mockDeclineState);
    }

    @Test
    void testHandleMessageAgreeShouldMoveToNextState() {
        when(mockAgreeState.getOpeningMessage()).thenReturn(SAMPLE_RESPONSE);
        String response = healthProtocolState.handleMessage(AGREE_MESSAGE);
        Assertions.assertEquals(transportUser.getCurrentState(), mockAgreeState);
        Assertions.assertEquals(SAMPLE_RESPONSE, response);
    }

    @Test
    void testHandleMessageDisagreeShouldMoveToDeclinedState() {
        String response = healthProtocolState.handleMessage(DISAGREE_MESSAGE);
        Assertions.assertEquals(transportUser.getCurrentState(), mockDeclineState);
        Assertions.assertEquals(DECLINE_REPLY, response);
    }

    @Test
    void testInvalidMessage() {
        String response = healthProtocolState.handleMessage(INVALID_MESSAGE);
        Assertions.assertEquals(INVALID_REPLY, response);
    }

    @Test
    void testOpeningMessage() {
        Assertions.assertEquals(OPENING_MESSAGE, healthProtocolState.getOpeningMessage());
    }

}
