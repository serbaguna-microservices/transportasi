package csui.serbagunabot.transportasi.chat.states.driver;

import csui.serbagunabot.transportasi.chat.states.DriverState;
import csui.serbagunabot.transportasi.model.Driver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DriverInputNameStateTest {

    @Mock
    DriverState mockState;

    private Driver driver;
    private DriverInputNameState driverInputNameState;

    private static final String SAMPLE_NAME = "ABC XYZ";
    private static final String OPENING_REPLY = "Terima kasih untuk keinginan anda"
        + " bergabung dengan kami.\n"
        + "Silahkan input nama lengkap anda untuk melanjutkan pendaftaran\n"
        + "Contoh: '!driver John Doe'";

    @BeforeEach
    void setUp() {
        driver = new Driver();
        driverInputNameState = new DriverInputNameState(driver);
        driverInputNameState.setNextState(mockState);
    }

    @Test
    void testHandleMessageShouldSaveName() {
        driverInputNameState.handleMessage(SAMPLE_NAME);
        Assertions.assertEquals(SAMPLE_NAME, driver.getName());
    }

    @Test
    void testGetOpeningMessage() {
        String response = driverInputNameState.getOpeningMessage();
        Assertions.assertEquals(OPENING_REPLY, response);
    }

}
